export const environment = {
    production: true,
    apiBaseUrl: "http://hrapi.sihatku.us/api/",
    ENCRYPT_KEY: "16023647361217681889",
    apiEHRBaseUrl: "https://sihaktuehrapi.azurewebsites.net/",
    apiEHRSubDomain: "sihatkuehr",
    SERVER_URL: "http://apiohs.muc-academy.com",
};
