import { InjectionToken } from '@angular/core';

export class AppConfig {
    apiBaseUrl: string
    ENCRYPT_KEY: string
    apiEHRBaseUrl: string
    mucSubdomain: string
    apiEHRSubDomain: string
    OHS_SERVER_URL: string
    SERVER_URL: string
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');