import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { CommonService } from '../../../core/service/common.service';
import { CommonFunction } from '../../../core/classes/common-function';

@Component({
    selector: 'auth-sign-in',
    templateUrl: './sign-in.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthSignInComponent implements OnInit {
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    signInForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private commonService: CommonService,
        private commonFunction: CommonFunction
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.signInForm = this._formBuilder.group({
            // username: ['Shyam@', [Validators.required]],
            // password: ['Shyam@2021', Validators.required],
            username: ['Admin', [Validators.required]],
            password: ['Admin@123', Validators.required],
            rememberMe: ['']
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    signIn(): void {
        // Return if the form is invalid
        if (this.signInForm.invalid) {
            return;
        } else {
            let data: any = {
                "UserName": this.signInForm.value.username,
                "Email": "",
                "Passwd": this.signInForm.value.password,
                'HRUserYN': 0
            };

            this.commonService.login('userlogin', data).subscribe((next: any) => {
                if (next.response.length > 0) {
                    this.commonFunction.setLoginData(next.response[0]);
                    // Navigate to the redirect url
                    this._router.navigateByUrl('dashboard');
                } else {
                    this.onError(next.message);
                }
            }, (error) => {
                this.onError('Something went wrong. Please try again.');
            });
        }

        // Disable the form
        this.signInForm.disable();

        // Hide the alert
        this.showAlert = false;
    }


    onError(message): void {
        this.alert = {
            type: 'error',
            message: message
        };
        this.signInForm.enable();

        // Reset the form
        this.signInNgForm.resetForm();

        // Show the alert
        this.showAlert = true;
    }
}
