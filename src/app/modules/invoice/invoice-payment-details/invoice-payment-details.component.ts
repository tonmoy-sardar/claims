import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-invoice-payment-details',
  templateUrl: './invoice-payment-details.component.html',
  styleUrls: ['./invoice-payment-details.component.scss']
})
export class InvoicePaymentDetailsComponent implements OnInit {
  public selectedPaymentMethod;
  public paymentMethods;
  public clinicPaymentDetails;
  public invoice;
  public curDate = new Date();
  public remarksMaxAllowedLength = 1000;

  public invoicePaymentForm = this.formBuilder.group({
    transactionId: ['', [Validators.required, Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$")]],
    chequeNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$")]],
    CheckIssuedDate: ['', [Validators.required]],
    amount: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
    remarks: ['', [Validators.maxLength(this.remarksMaxAllowedLength)]],
  });

  constructor(
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.getPaymentMethodTypes();
    this.invoice = this.data;
    this.getClinicPaymentDetails();
  }


  getPaymentMethodTypes(): void {
    this.commonService.httpViaPost('getpaymenttypelist', {}).subscribe((next: any) => {
      if (next.status === 1) {
        this.paymentMethods = next.response;
      }
    });
  }

  getClinicPaymentDetails(): void {
    this.commonService.httpViaPost('getclinicpaymentdetails', { "InvoiceId": "3" }).subscribe((next: any) => {
      if (next.status === 1) {
        this.clinicPaymentDetails = next.response;
      }
    });
  }

  updatePaymentDetails(): void {
    this.invoicePaymentForm.markAllAsTouched();
    const request = {
      'InvoiceId': this.invoice.InvoiceId,
      'PaymentAmount': this.invoicePaymentForm.controls.amount.value,
      "ReceivedAmount": "",
      'PaymentTypeId': this.selectedPaymentMethod.PTID,
      'TransectionId': this.invoicePaymentForm.controls.transactionId.value,
      'CheckNo': this.invoicePaymentForm.controls.chequeNumber.value,
      'CheckIssuedDate': this.invoicePaymentForm.controls.CheckIssuedDate.value,
      "IFSCCode": "",
      "CheckPayedBy": "",
      "BankName": "",
      'LoginUser': this.commonService.loggedInUserId,
      'PayedByHRYN': 'N'
    };
    if (this.invoicePaymentForm.valid) {
      this.commonService.httpViaPost('addupdateclinicpaymenthistory', request).subscribe((next: any) => {
        if (next.status == 1) {
          const dialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Success', message: 'Payment Details Updated Successfully' }
          });

          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.dialogRef.close({ 'paymentDetails': this.selectedPaymentMethod.PTName, 'submitted': true, 'remarks': this.invoicePaymentForm.controls.remarks.value.trim() });
          });
        }
      });
    } else {
      console.warn("Form is Invalid.");
    }
  }

  changePaymentType(radio): void {
    this.invoicePaymentForm.patchValue({
      amount: '',
      transactionId: '',
      chequeNumber: ''
    });
    this.invoicePaymentForm.markAsUntouched();
    this.invoicePaymentForm.markAsPristine();
    if (radio.value && radio.value.PTID && radio.value.PTName === 'Cheque') {
      this.invoicePaymentForm.controls.chequeNumber.setValidators([Validators.required, Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$")]);
      this.invoicePaymentForm.controls.chequeNumber.updateValueAndValidity();
      this.invoicePaymentForm.controls.transactionId.clearValidators();
      this.invoicePaymentForm.controls.transactionId.updateValueAndValidity();
      this.invoicePaymentForm.controls.CheckIssuedDate.clearValidators();
      this.invoicePaymentForm.controls.CheckIssuedDate.updateValueAndValidity();
    } else {
      this.invoicePaymentForm.controls.transactionId.setValidators([Validators.required, Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$")]);
      this.invoicePaymentForm.controls.transactionId.updateValueAndValidity();
      this.invoicePaymentForm.controls.chequeNumber.clearValidators();
      this.invoicePaymentForm.controls.chequeNumber.updateValueAndValidity();
      this.invoicePaymentForm.controls.CheckIssuedDate.clearValidators();
      this.invoicePaymentForm.controls.CheckIssuedDate.updateValueAndValidity();
    }
  }


  closePopup(): void {
    this.dialogRef.close({ 'paymentDetails': '', 'submitted': false });
  }

}
