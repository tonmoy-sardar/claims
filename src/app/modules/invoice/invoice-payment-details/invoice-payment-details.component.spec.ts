import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePaymentDetailsComponent } from './invoice-payment-details.component';

describe('InvoicePaymentDetailsComponent', () => {
  let component: InvoicePaymentDetailsComponent;
  let fixture: ComponentFixture<InvoicePaymentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoicePaymentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePaymentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
