import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { MaterialModule } from 'app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceCompanyComponent } from './invoice-company/invoice-company.component';
import { InvoiceClinicComponent } from './invoice-clinic/invoice-clinic.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { InvoicePaymentDetailsComponent } from './invoice-payment-details/invoice-payment-details.component';
import { InvoicePaymentDetailsReadOnlyComponent } from './invoice-payment-details-read-only/invoice-payment-details-read-only.component';



@NgModule({
  declarations: [
    InvoiceCompanyComponent,
    InvoiceClinicComponent,
    InvoiceDetailsComponent,
    InvoicePaymentDetailsComponent,
    InvoicePaymentDetailsReadOnlyComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InvoiceRoutingModule,
    SharedModule
  ]
})
export class InvoiceModule { }
