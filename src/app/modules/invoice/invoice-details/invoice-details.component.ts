import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss']
})
export class InvoiceDetailsComponent implements OnInit {
  displayedColumns: string[] = ['claimId', 'invoiceDate','patientName', 'clinicName', 'invoiceAmount'];
  displayedColumns2: string[] = ['InvoiceId', 'PTName', 'PaymaentDate','CheckIssuedDate' ,'TransectionId' ,'CheckNo','PaymentAmount'];
  public invoice;
  public invoiceDetails = [];
  public paymentDetails = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService
  ) { }


  ngOnInit(): any {
    console.warn('claim data: ' + JSON.stringify(this.data));
    this.invoice = this.data.invoice;
    this.getInvoiceDetails();
    if(this.data.type === 'company'){
      this.getPaymentMethodTypes();
    }else{
      this.getclinicPaymentDetails();
    }
  }

  getPaymentMethodTypes(): void {
    this.commonService.httpViaPost('getpaymentdetails', {InvoiceId:this.invoice.InvoiceId}).subscribe((next: any) => {
      if (next.status === 1) {
        this.paymentDetails = next.response;
        console.log(this.paymentDetails)
      }
    });
  }

  getclinicPaymentDetails(): void {
    this.commonService.httpViaPost('getclinicpaymentdetails', {InvoiceId:this.invoice.InvoiceId}).subscribe((next: any) => {
      if (next.status === 1) {
        this.paymentDetails = next.response;
        console.log(this.paymentDetails)
      }
    });
  }

  getInvoiceDetails(): void {
    const request = {
      'InvoiceId': this.invoice.InvoiceId,
      'Flag': this.data.flag
    };
    this.commonService.httpViaPost('getinvoiceclaimsdetails', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.invoiceDetails = next.response;
      }
    });
  }

  closePopup(): void {
    this.dialogRef.close();
  }

  getAmount(element){
    if(element.ClaimAmount){
      return element.ClaimAmount;
    }else{
      element.PerticularClaimAmount;
    }
  }

}
