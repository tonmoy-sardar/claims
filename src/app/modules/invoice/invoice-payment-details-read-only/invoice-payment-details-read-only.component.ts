import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { CommonFunction } from 'app/core/classes/common-function';

@Component({
  selector: 'app-invoice-payment-details-read-only',
  templateUrl: './invoice-payment-details-read-only.component.html',
  styleUrls: ['./invoice-payment-details-read-only.component.scss']
})
export class InvoicePaymentDetailsReadOnlyComponent implements OnInit {

  public remarks = "";

  constructor( private commonService: CommonService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    private matDialog: MatDialog,
    private commonFunction: CommonFunction,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    public invoiceDetails = [];
    displayedColumns: string[] = ['InvoiceId', 'PTName', 'PaymaentDate', 'TransectionId' ,'CheckNo','PaymentAmount', 'CheckIssuedDate'];

  ngOnInit(): void {
    this.getPaymentMethodTypes();
    console.log(this.data);
  }

  getPaymentMethodTypes(): void {
    this.commonService.httpViaPost('getpaymentdetails', {InvoiceId:this.data.InvoiceId}).subscribe((next: any) => {
      if (next.status === 1) {
        this.invoiceDetails = next.response;
      }
    });
  }

  confirmInvoice(status): void {
    let loggedInUser: any = this.commonFunction.getLoginData().data;
    const data = {
      "Flag" : "2",
      "InvoiceId": this.data.InvoiceId,
      "Status": status,
      "UserId": loggedInUser.UserName,
      "PayedByCompanyYN":"",
      "Remarks": this.remarks
      };

    this.commonService.httpViaPost('updateinvoicepaymentstatus ', data).subscribe((next: any) => {
      if (next.status == 1) {
        const dialogRef = this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Success', message: 'Payment Details Updated Successfully' }
        });

        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.dialogRef.close({ 'paymentDetails': next.results, 'submitted': true });
        });
      }
    });
  }

  closePopup(): void {
    this.dialogRef.close({ 'paymentDetails': '', 'submitted': false });
  }

  getFormattedDate(date): void {
    console.log(date);
  }

}
