import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePaymentDetailsReadOnlyComponent } from './invoice-payment-details-read-only.component';

describe('InvoicePaymentDetailsReadOnlyComponent', () => {
  let component: InvoicePaymentDetailsReadOnlyComponent;
  let fixture: ComponentFixture<InvoicePaymentDetailsReadOnlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoicePaymentDetailsReadOnlyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePaymentDetailsReadOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
