import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { InvoiceDetailsComponent } from '../invoice-details/invoice-details.component';
import { InvoicePaymentDetailsComponent } from '../invoice-payment-details/invoice-payment-details.component';
import moment from 'moment';

@Component({
  selector: 'app-invoice-clinic',
  templateUrl: './invoice-clinic.component.html',
  styleUrls: ['./invoice-clinic.component.scss']
})
export class InvoiceClinicComponent implements OnInit {
  public invoiceData = [];
  public clinicList = [];

  public pageCount = 1;
  public showNext;
  public recordCount;
  public totalPageCount = 0;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public pageSize = 10;
  public sortOrderVal = 0;
  public sortedColumn: string;

  public date: any = new Date();
  public showLoader = true;
  public min_date = null;

  public searchField: any = {
    StartDate: "",
    EndDate: "",
    ClinicCode: "",
    PaymentStatus: '',
    IsAscending: '0',
    OrderByColumnName: '',
    PageIndex: '1'
  };

  public searchClinicAutoComplete = new FormControl();
  public filteredClinicList: Observable<string[]>;
  $clinicList: Subject<any>;

  constructor(private commonFunction: CommonFunction,
    private matDialog: MatDialog,
    private commonService: CommonService) {
    this.$clinicList = new Subject();
  }

  ngOnInit(): void {
    this.$clinicList.subscribe((next: any) => {
      this.getFilteredClinics();
    });
    this.getClinicList();
    this.sortData('InvoiceDate', 'desc');
    this.min_date = new Date(2020, 0, 1);
  }

  getFilteredClinics(): any {
    this.filteredClinicList = this.searchClinicAutoComplete.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filterClinicList(value))
      );
  }

  filterClinicList(value: any): any {
    let filterValue;
    if (typeof (value) === 'object') {
      this.searchField.ClinicCode = value.clinicCode;
      filterValue = value && value.clinicName ? value.clinicName.toLowerCase() : '';
    } else {
      this.searchField.ClinicCode = '';
      filterValue = value.trim().toLowerCase();;
    }
    return this.clinicList.filter(option => option.clinicName.toLowerCase().includes(filterValue));
  }

  displayClinicFn(clinic: any): string {
    return clinic && clinic.clinicName ? clinic.clinicName : '';
  }

  getClinicList(): void {
    this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
      if (next.status === 1 && next.response.length > 0) {
        this.clinicList = next.response;
        this.$clinicList.next(this.clinicList);
      } else {
        this.getDummyClinicData();
      }
    }, (err) => {
      console.warn('getclinicname API Failed.');
      this.getDummyClinicData();
    });
  }

  filterInvoiceData(): void {
    this.pageCount = 1;
    this.getInvoiceData();
  }

  getInvoiceData(): void {
    this.showLoader = true;
    this.searchField.PageIndex = this.pageCount;
    this.requestInvoiceData().subscribe((data: any) => {
      if (data.status === 1) {
        this.invoiceData = data.response;
        this.showLoader = false;
        if (this.invoiceData.length > 0) {
          this.recordCount = this.invoiceData[0].TotalRecord;
          if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
            this.showNext = true;
          } else {
            this.showNext = false;
          }
        } else {
          this.showNext = false;
          this.recordCount = 0;
        }
        this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
      }
    }, (err) => {
      console.warn('getmcreport API failed');
    });
  }

  requestInvoiceData(): Observable<any> {
    const searchField: any = Object.assign({}, this.searchField);

    if (this.searchField.StartDate !== '') {
      searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
    }

    if (this.searchField.EndDate !== '') {
      searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
    }
    searchField.CompanyId = this.commonService.companyId;

    return this.commonService.httpViaPost('getinvoicepayabledetails', searchField);
  }

  requestInvoiceDataForExport(): Observable<any> {
    const searchField: any = Object.assign({}, this.searchField);

    if (this.searchField.StartDate && this.searchField.StartDate !== '') {
      searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
    }

    if (this.searchField.EndDate && this.searchField.EndDate !== '') {
      searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
    }
    searchField.CompanyId = this.commonService.companyId;

    return this.commonService.httpViaPost('ExportExcelForClinic', searchField);
  }

  changeEndDate(): any {
    if (this.searchField.StartDate == '') {
      return this.min_date
    } else {
      return this.searchField.StartDate;
    }
  }

  resetSearch(): void {
    this.resetPageIndex = true;
    console.warn(this.searchField);

    this.pageCount = 1;
    this.searchField.StartDate = '';
    this.searchField.EndDate = '';
    this.searchField.ClinicCode = '';
    this.searchField.PaymentStatus = '';
    this.searchField.OrderByColumnName = 'InvoiceDate';
    this.searchField.IsAscending = 0;
    this.searchField.PageIndex = 1;


    this.getInvoiceData();
  }

  sortData(sortColumn: string, order: string): void {
    this.resetPageIndex = true;
    this.pageCount = 1;

    if (order === 'asc') {
      this.sortOrderVal = this.searchField.IsAscending = 0;
    } else if (order === 'desc') {
      this.sortOrderVal = this.searchField.IsAscending = 1;
    }
    this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
    this.getInvoiceData();
  }

  changePage(pageNumber): void {
    // if (str === 'next') {
    //   this.pageCount += 1;
    //   this.showNext = false;
    // } else {
    //   this.pageCount -= 1;
    // }
    // this.getInvoiceData();
    this.resetPageIndex = false;
    if (this.pageCount !== pageNumber) {
      this.pageCount = pageNumber;
      this.getInvoiceData();
    }
  }

  onPaidClick(invoice: any): void {
    const fulfillmentDialogRef = this.matDialog.open(InvoicePaymentDetailsComponent, {
      width: '650px',
      height: '650px',
      data: invoice,
      disableClose: true
    });

    fulfillmentDialogRef.afterClosed().subscribe((result) => {
      console.warn(result);
      if (result.submitted) {
        this.updatePaidStatus(invoice, '1', result.remarks);
      } else {
        console.log('The Popup was closed and not submitted');
      }
    });
  }

  onUnPaidClick(invoice: any): void {
    this.updatePaidStatus(invoice, '0');
  }

  updatePaidStatus(invoice: any, status: string, remarks: string = ''): void {
    let loggedInUser: any = this.commonFunction.getLoginData().data;
    const request = {
      'UserId': loggedInUser.UserName,
      'Status': status,
      'InvoiceId': invoice.InvoiceId.toString(),
      'CompanyId': this.commonService.companyId,
      'Flag': '1', //value 1 represents clinic
      'Remarks': remarks
    };
    this.commonService.httpViaPost('updateinvoicepaymentstatus', request).subscribe((next: any) => {
      if (next.status === 1) {
        const dialogRef = this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Success', message: 'Invoice Status Updated Successfully' }
        });

        dialogRef.afterClosed().subscribe((result) => {
          this.filterInvoiceData();
          console.log('The dialog was closed', result);
        });
      }
    });
  }

  showInvoiceDetails(invoice: any): void {
    const dialogRef = this.matDialog.open(InvoiceDetailsComponent, {
      width: '950px',
      height: '550px',
      data: { invoice, 'flag': '1', type: 'clinic' },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  exportToExcel(): void {
    this.searchField.PageIndex = 0;
    const exportObj = [];
    this.requestInvoiceDataForExport().subscribe((data) => {
      if (data.status === 1 && data.response.length > 0) {
        data.response.forEach((report, index) => {
          let paymentDetailsColumns = {};
          if (report.PaymentDetails && report.PaymentDetails.length > 0) {
            report.PaymentDetails.forEach((payment, paymentIndex) => {
              paymentIndex += 1;
              paymentDetailsColumns[`Payment Type ${paymentIndex}`] = payment?.ptname;
              paymentDetailsColumns[`Payment Date ${paymentIndex}`] = this.commonService.getFormattedDate(payment?.paymaentdate);
              paymentDetailsColumns[`Cheque Issue Date ${paymentIndex}`] = this.commonService.getFormattedDate(payment?.checkissueddate);
              paymentDetailsColumns[`Transaction Id ${paymentIndex}`] = payment?.transectionid;
              paymentDetailsColumns[`Cheque Number ${paymentIndex}`] = payment?.checkno;
            });
          }

          const element = {
            'Invoice No.': report.InvoiceNo,
            'Invoice Date': this.commonService.getFormattedDate(report.InvoiceDate),
            'Clinic Name': report.ClinicName,
            'Patient Name': report.PatientName,
            'Visit Date': this.commonService.getFormattedDate(report.visitDt),
            'IC Number': report.ICNo,
            'Company Name': report.companyName,
            'Claim Amount(RM)': report.ClaimAmount ? `RM ${report.ClaimAmount?.toString()}` : `RM 0`,
            'Remaining Balance(RM)': report.RemainingBalance ? `RM ${report.RemainingBalance?.toString()}` : `RM 0`,
            'Payed Amount(RM)': report.PayedAmount ? `RM ${report.PayedAmount?.toString()}` : `RM 0`,
            'Payment Status': report.PaymentStatus,
            'Remarks': report.Remarks,
          };


          let combinedElement = Object.assign(element, paymentDetailsColumns);
          exportObj.push(combinedElement);
        });
        this.commonService.downloadExcel(exportObj, 'Clinic_Invoice_Report', 'Clinic_Invoice_Report.xlsx');
      }
    });
  }

  getDummyInvoiceData() {
    this.invoiceData = [
      {
        "InvoiceDate": "2021-10-26T00:00:00",
        "InvoiceNumber": "MUC002",
        "InvoiceAmount": 25.0,
        "CompanyName": "SIHATKU-TPA",
        "PaidOrUnpaid": 10
      }
    ];
  }

  getDummyClinicData(): void {
    this.clinicList = [
      {
        "clinicName": "D01"
      },
      {
        "clinicName": "D02"
      },
      {
        "clinicName": "D03"
      },
      {
        "clinicName": "D04"
      },
      {
        "clinicName": "D05"
      },
      {
        "clinicName": "Tst001"
      },
      {
        "clinicName": "Tst002"
      }
    ];
    this.$clinicList.next(this.clinicList);
  }
}
