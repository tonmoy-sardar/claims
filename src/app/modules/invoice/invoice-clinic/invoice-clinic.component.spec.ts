import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceClinicComponent } from './invoice-clinic.component';

describe('InvoiceClinicComponent', () => {
  let component: InvoiceClinicComponent;
  let fixture: ComponentFixture<InvoiceClinicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceClinicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceClinicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
