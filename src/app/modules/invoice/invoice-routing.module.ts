import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceClinicComponent } from './invoice-clinic/invoice-clinic.component';
import { InvoiceCompanyComponent } from './invoice-company/invoice-company.component';


const routes: Routes = [
    { path: 'clinic', component: InvoiceClinicComponent },
    { path: 'company', component: InvoiceCompanyComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InvoiceRoutingModule { }
