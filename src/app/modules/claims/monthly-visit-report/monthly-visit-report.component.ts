import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';
import { CommonFunction } from '../../../core/classes/common-function';
import { CommonService } from '../../../core/service/common.service';

@Component({
    selector: 'monthly-visit-report',
    templateUrl: './monthly-visit-report.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MonthlyVisitReportComponent implements OnInit {

    public employeeData: any = {
        dataSource: [],
        dataCount: 0,
        page: 0
    };

    startDate: any = new Date();
    endDate: any = new Date();
    maxDate: any = new Date();

    public department: any = [];
    public location: any = [];

    public searchField: any = {
        year: "",
        month: "",
        ClinicId: "",
        Flag: 1
    };
    public clinicData: any;

    constructor(
        private _projectService: ProjectService,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
    }

    ngOnInit() {
        let loginData = this.commonFunction.getLoginData();
        this.searchField.ClinicId = loginData.data.CompanyId;
        this.startDate = 1;
        this.getClinic();
    }

    searchEmployee() {
        let searchField: any = this.searchField;
        console.log("Data : ", searchField);
        searchField.CompanyId = this.commonService.companyId;

        this.commonService.httpViaPost('getvisitreport', searchField).subscribe((next: any) => {
            console.log("Next : ", next);
            if (next.status == 1) {
                this.employeeData.dataSource = next.response;
            }
        });
    }

    resetSort() {

    }

    getClinic() {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            console.log("Next : ", next);
            if (next.status == 1) {
                this.clinicData = next.response;
                console.log("this.clinicData : ", this.clinicData);
            }
        });
    }

}
