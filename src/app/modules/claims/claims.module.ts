import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from '../../core/service/common.service';

import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../my-date-formats';

import { MonthlyVisitReportComponent } from 'app/modules/claims/monthly-visit-report/monthly-visit-report.component';
import { VisitReportComponent } from 'app/modules/claims/visit-report/visit-report.component';
import { AutoProcessingComponent} from 'app/modules/claims/auto-processing/auto-processing.component';
import { ManualProcessingComponent } from './manual-processing/manual-processing.component';
import { ClaimDetailsComponent } from './claim-details/claim-details.component';
import { ClaimRejectReasonComponent } from './claim-reject-reason/claim-reject-reason.component';
import { SharedModule } from 'app/shared/shared.module';

const exampleRoutes: Route[] = [
    {
        path     : 'monthly-visit-report',
        component: MonthlyVisitReportComponent
    },
    {
        path     : 'visit-report',
        component: VisitReportComponent
    },
    {
        path     : 'auto-processing',
        component: AutoProcessingComponent
    },
    {
        path     : 'manual-processing/:status',
        component: ManualProcessingComponent
    },
    {
        path     : 'manual-processing',
        component: ManualProcessingComponent
    }
];

@NgModule({
    declarations: [
        MonthlyVisitReportComponent,
        VisitReportComponent,
        AutoProcessingComponent,
        ManualProcessingComponent,
        ClaimDetailsComponent,
        ClaimRejectReasonComponent
    ],
    imports     : [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forChild(exampleRoutes)
    ],
    providers: [
        CommonService,
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class ClaimsModule
{
}
