import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { CommonFunction } from '../../../core/classes/common-function';
import { CommonService } from '../../../core/service/common.service';

@Component({
    selector     : 'mc-report',
    templateUrl  : './mc-report.component.html',
    encapsulation: ViewEncapsulation.None
})
export class McReportComponent implements OnInit
{
    public employeeData: any = {
        displayedColumns: [],
        dataSource: [],
        totalCount: 0,
        errorCount: 0,

        allData: [],
        allDataColumns: [],
        page: 0,
        totalData: 0
    };

    startDate: any = new Date();
    endDate: any = new Date();
    
    public department: any = [];
    public location: any = [];

    public clinicData: any;
    public searchField: any = {
        StartDate: "",
        EndDate: "",
        ClinicId: "",
        PageIndex: 1
    };

    /**
     * Constructor
     */
    constructor(
        private commonFunction: CommonFunction,
        private commonService: CommonService
    )
    {
    }

    ngOnInit() {
        let loginData = this.commonFunction.getLoginData();
        this.searchField.ClinicId = loginData.data.CompanyId;

        this.getClinic();
    }

    searchEmployee() {
        let loginData = this.commonFunction.getLoginData();
        this.searchField.ClinicId = loginData.data.CompanyId;

        let searchField: any = {
            StartDate: "",
            EndDate: "",
            ClinicId: this.searchField.ClinicId,
            PageIndex: 1
        };
        if(this.searchField.StartDate != '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if(this.searchField.EndDate != '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        console.log("Data : ", searchField);
        searchField.CompanyId = this.commonService.companyId;

        this.commonService.httpViaPost('getmcreport', searchField).subscribe((next: any) => {
            console.log("Next : ", next);
            if (next.status == 1) {
                this.employeeData.dataSource = next.response;
            }
        });
    }

    resetSort() {
        
    }

    getClinic() {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            console.log("Next : ", next);
            if (next.status == 1) {
                this.clinicData = next.response;
                console.log("this.clinicData : ", this.clinicData);
            }
        });
    }

}
