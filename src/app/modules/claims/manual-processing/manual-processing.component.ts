import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ValidationMessages } from 'app/core/classes/validation-messages';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { Observable, of, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CommonFunction } from '../../../core/classes/common-function';
import { CommonService } from '../../../core/service/common.service';
import { ClaimDetailsComponent } from '../claim-details/claim-details.component';

function autocompleteObjectValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control && control?.value?.length > 0) {
            if (control?.value?.split(" ").join("") === '') {
                return { 'emptyStringError': { value: control.value } }
            } else {
                const regexp = new RegExp('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$');
                const test = regexp.test(control.value);
                if (!test)
                    return { 'pattern': { value: control.value } };
                else {
                    return { 'emptySelection': { value: control.value } };
                }
            }
        }
        return null;
    }
}

@Component({
    selector: 'app-manual-processing',
    templateUrl: './manual-processing.component.html',
    styleUrls: ['./manual-processing.component.scss'],
    providers: [DatePipe]
})
export class ManualProcessingComponent implements OnInit {
    public apiEHRSubDomain: string = this.commonService.appConfig.apiEHRSubDomain;
    public apiEHRBaseUrl: string = this.commonService.appConfig.apiEHRBaseUrl;

    public searchClinicAutoComplete = new FormControl('',
        { validators: [autocompleteObjectValidator()] });
    public searchCompanyAutoComplete = new FormControl('');

    public claimData = [];
    public pageIndex = 1;
    public pageSize = 10;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public showNext;
    public showPrevious;
    public employeeData: any = {
        dataSource: [],
        dataCount: 0,
        page: 0
    };

    startDate: any = new Date();
    endDate: any = new Date();
    maxDate: any = new Date();


    public clinicList;
    public companyList = [];
    public sortOrderVal = 0; //: string;
    public sortedColumn = '';
    public department: any = [];
    public location: any = [];
    public min_date = null;

    public searchField: any = {
        StartDate: '',
        EndDate: '',
        CompanyName: '',
        ClinicCode: '',
        IsAscending: '0',
        OrderByColumnName: '',
        ClaimsType: 'ManualProcessing',
        ClaimStatus: '',
        EmployeeName: '',
        VisitDate: '',
        PageIndex: '1'
    };

    public filteredClinicList: Observable<string[]>;
    public filteredCompanyList: Observable<string[]>;
    $clinicList: Subject<any>;
    $companyList: Subject<any>;

    public validation_msgs = [];
    public showLoader = true;
    private routeParam: any

    constructor(
        private validationMessages: ValidationMessages,
        private datepipe: DatePipe,
        private matDialog: MatDialog,
        private commonFunction: CommonFunction,
        private commonService: CommonService,
        private dataRoute: ActivatedRoute
    ) {
        this.$clinicList = new Subject();
        this.$companyList = new Subject();
    }

    ngOnInit(): void {

        this.routeParam = this.dataRoute.params.subscribe(params => {
            if (params['status'] && params['status'].length > 0) {
                this.searchField.ClaimStatus = params['status'];
            }
        });

        this.validation_msgs = this.validationMessages.messages;
        this.$clinicList.subscribe((next: any) => {
            this.getFilteredClinics();
        });
        this.$companyList.subscribe((next: any) => {
            this.getFilteredCompanies();
        });

        this.getClinicList();
        this.getCompanyList();
        this.searchClaim();
        this.sortData('VisitDate', 'desc');
        this.min_date = new Date(2020, 0, 1);
    }

    getFilteredCompanies(): any {
        this.filteredCompanyList = this.searchCompanyAutoComplete.valueChanges
            .pipe(
                startWith(''),
                map(value => this.filterCompanyList(value))
            );
    }

    getFilteredClinics(): any {
        this.filteredClinicList = this.searchClinicAutoComplete.valueChanges
            .pipe(
                startWith(''),
                map(value => this.filterClinicList(value))
            );
    }

    filterClinicList(value: any): any {
        let filterValue;
        if (typeof (value) === 'object') {
            this.searchField.ClinicCode = value.clinicCode;
            filterValue = value && value.clinicName ? value.clinicName.toLowerCase() : '';
        } else {
            this.searchField.ClinicCode = '';
            filterValue = value.trim().toLowerCase();;
        }
        return this.clinicList.filter(option => option.clinicName.toLowerCase().includes(filterValue));
    }

    displayClinicFn(clinic: any): string {
        return clinic && clinic.clinicName ? clinic.clinicName : '';
    }

    filterCompanyList(value: string): any {
        const filterValue = value.trim().toLowerCase();

        return this.companyList.filter(option => option.CoName.toLowerCase().includes(filterValue));
    }

    getClinicList(): void {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.clinicList = next.response;
                this.$clinicList.next(this.clinicList);
            }
        }, (err) => {
            console.warn('getclinicname API Failed.');
            this.clinicList = this.getDummyClinicData();
            this.$clinicList.next(this.clinicList);
        });
    }

    getCompanyList(): void {
        this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyList = next.response;
                this.$companyList.next(this.companyList);
            }
        }, (err) => {
            console.warn('getcompanyList API Failed.');
        });
    }

    searchClaim(): void {
        this.pageIndex = 1;
        this.getClaimsData();
    }

    getClaimsData(): void {
        this.showLoader = true;
        this.searchField.PageIndex = this.pageIndex;
        const searchRequest: any = Object.assign({}, this.searchField);

        if (this.searchField.StartDate && this.searchField.StartDate !== '') {
            searchRequest.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if (this.searchField.EndDate && this.searchField.EndDate !== '') {
            searchRequest.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }


        searchRequest.ClinicCode = this.searchField.ClinicCode?.trim();
        searchRequest.CompanyName = this.searchField.CompanyName?.trim();
        searchRequest.LoginUserId = this.commonService.loggedInUserId;
        searchRequest.CompanyId = this.commonService.companyId;

        this.commonService.httpViaPost('getvisitlistreport', searchRequest).subscribe((next: any) => {
            if (next.status === 1) {
                this.showLoader = false;
                this.claimData = next.response;

                if (this.claimData.length > 0) {
                    this.recordCount = this.claimData[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageIndex * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        });
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageIndex = 1;
        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;

        this.getClaimsData();
    }

    resetSearch(): void {
        this.resetPageIndex = true;
        this.searchClinicAutoComplete.setValue('');

        this.pageIndex = 1;
        this.sortedColumn = 'VisitDate';
        this.searchField.StartDate = '';
        this.searchField.EndDate = '';
        this.searchField.VisitDate = '';
        this.searchField.CompanyName = '';
        this.searchField.ClinicCode = '';
        this.searchField.ClaimsType = 'ManualProcessing';
        this.searchField.ClaimStatus = '';
        this.searchField.EmployeeName = '';
        this.searchField.OrderByColumnName = 'VisitDate';
        this.searchField.IsAscending = 0;
        this.searchField.PageIndex = 1;

        this.getClaimsData();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageIndex += 1;
        //     this.showNext = false;
        // } else {
        //     this.pageIndex -= 1;
        // }
        // this.getClaimsData();
        this.resetPageIndex = false;
        if (this.pageIndex !== pageNumber) {
            this.pageIndex = pageNumber;
            this.getClaimsData();
        }
    }

    showClaimDetails(claim: any): void {
        const dialogRef = this.matDialog.open(ClaimDetailsComponent, {
            width: '950px',
            height: '550px',
            data: { 'showActionButtons': true, claim },
            disableClose: true
        });

        dialogRef.afterClosed().subscribe((result) => {
            this.searchClaim();
            console.log('The dialog was closed', result);
        });
    }

    approveClaim(claim: any): void {
        let loggedInUser: any = this.commonFunction.getLoginData().data;
        const request = {
            'ClaimId': claim.Id,
            'ClaimStatus': 1,
            'CreatedUser': loggedInUser.UserName,
            'CompanyId': this.commonService.companyId
        };
        this.commonService.httpViaPost('updateclaimsstatus', request).subscribe((next: any) => {
            if (next.status === 1) {
                const req = {
                    'MyKad': claim.ICNum,
                    'VisitDate': this.datepipe.transform(claim.visitDt, 'dd/MM/yyyy'),
                    'ClimStatus': '1',
                };
                const url = `${this.apiEHRBaseUrl}panel/UpadeClaimStatusHRToEHR?subdomain=${this.apiEHRSubDomain}`;
                this.commonService.httpPostWithoutHeaders(url, req, true).subscribe((response: any) => {
                    if (response.Status === 200) {

                        const dialogRef = this.matDialog.open(AlertComponent, {
                            width: '250px'
                        });

                        dialogRef.afterClosed().subscribe((result) => {
                            this.searchClaim();
                            console.log('The dialog was closed', result);
                        });
                    }
                });
            }
        });
    }

    changeEndDate(): any {
        if (this.searchField.StartDate == '') {
            return this.min_date
        } else {
            return this.searchField.StartDate;
        }
    }

    getDummyClinicData(): any {
        return [
            {
                'clinicName': 'D01'
            },
            {
                'clinicName': 'D02'
            },
            {
                'clinicName': 'D03'
            },
            {
                'clinicName': 'D04'
            },
            {
                'clinicName': 'D05'
            },
            {
                'clinicName': 'Tst001'
            },
            {
                'clinicName': 'Tst002'
            }
        ];
    }
}
