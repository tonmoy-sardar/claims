import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualProcessingComponent } from './manual-processing.component';

describe('ManualProcessingComponent', () => {
  let component: ManualProcessingComponent;
  let fixture: ComponentFixture<ManualProcessingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManualProcessingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
