import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimRejectReasonComponent } from './claim-reject-reason.component';

describe('ClaimRejectReasonComponent', () => {
  let component: ClaimRejectReasonComponent;
  let fixture: ComponentFixture<ClaimRejectReasonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClaimRejectReasonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimRejectReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
