import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-claim-reject-reason',
  templateUrl: './claim-reject-reason.component.html',
  styleUrls: ['./claim-reject-reason.component.scss'],
  providers: [DatePipe]
})
export class ClaimRejectReasonComponent implements OnInit {
  public apiEHRSubDomain;
  public apiEHRBaseUrl;
  public rejectReason;
  public disableSubmitButton = true;

  constructor(
    private commonService: CommonService,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
      this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
    }

  ngOnInit(): void {
    this.rejectReason = this.data.ClaimRejectReason;
    if (this.rejectReason && this.rejectReason !== '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  updateClaimStatus(): void {
    this.data.ClaimRejectReason = this.rejectReason;
    this.dialogRef.close({ 'rejectReason': this.rejectReason, 'submitted': true });
  }

  changeReason(text): void {
    if (text.target.value && text.target.value != '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  closePopup(): void {
    this.dialogRef.close({ 'rejectReason': '', 'submitted': false });
  }
}
