import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent, DialogData } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { ClaimRejectReasonComponent } from '../claim-reject-reason/claim-reject-reason.component';

@Component({
    selector: 'app-claim-details',
    templateUrl: './claim-details.component.html',
    styleUrls: ['./claim-details.component.scss'],
    providers: [DatePipe]
})
export class ClaimDetailsComponent implements OnInit {
    public apiEHRSubDomain;
    public apiEHRBaseUrl;
    displayedColumns: string[] = ['description', 'invoiceAmount'];
    public showActionButtons = false;
    public claim;
    public diagnosisDetails = [];
    public employeeClaimRecords = [];
    public patientDetails;
    public loggedInUser;
    public totalAmount = 0;

    constructor(
        private datepipe: DatePipe,
        private matDialog: MatDialog,
        public dialogRef: MatDialogRef<any>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
        this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
        this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
    }


    ngOnInit(): any {
        this.loggedInUser = this.commonFunction.getLoginData().data;
        this.showActionButtons = this.data.showActionButtons;
        this.claim = this.data.claim;
        console.log(this.claim);
        this.getPatientDetails();
        this.getEmployeeClaimsRecord();
        this.getDiagnosisDetails();
    }

    getPatientDetails(): void {
        const request = {
            'EmployeeId': this.claim.EmployeeId,
            'CompanyId': this.commonService.companyId
        };
        this.commonService.httpViaPost('getpatientdetsils', request).subscribe((next: any) => {
            if (next.status === 1) {
                this.patientDetails = next.response[0];
            }
        });
    }

    getEmployeeClaimsRecord(): void {
        const request = {
            'EmployeeId': this.claim.EmployeeId,
            'CompanyId': this.commonService.companyId,
            'LoginUserId': this.commonService.loggedInUserId
        };
        this.commonService.httpViaPost('getclaimsdetsils', request).subscribe((next: any) => {
            if (next.status === 1) {
                this.employeeClaimRecords = next.response;
            }
        });
    }

    getDiagnosisDetails(): void {
        const request = {
            'ClaimId': this.claim.Id
        };
        this.commonService.httpViaPost('getinvoicelineitemdetails', request).subscribe((next: any) => {
            if (next.status === 1) {
                this.diagnosisDetails = next.response;
                if (next.response.length > 0) {
                    this.totalAmount = next.response[0].TotalAmmount;
                    this.diagnosisDetails.push({ 'LineDec': 'Total Amount', 'LineAmt': this.totalAmount, 'showInRed': true });
                }
            }
        });
    }

    updateClaimStatus(claimStatus: any, ehrClaimStatus: any): void {
        const request = {
            'ClaimId': this.claim.Id,
            'ClaimStatus': claimStatus,
            'CreatedUser': this.loggedInUser.UserName,
            'CompanyId': this.commonService.companyId,
            'ClaimRejectReason': this.claim.ClaimRejectReason
        };
        this.commonService.httpViaPost('updateclaimsstatus', request).subscribe((next: any) => {
            if (next.status === 1) {
                const req = {
                    'MyKad': this.claim.ICNum,
                    'VisitDate': this.datepipe.transform(this.claim.visitDt, 'dd/MM/yyyy'),
                    'ClimStatus': ehrClaimStatus,
                    'RejectReason': this.claim.ClaimRejectReason
                };
                const url = `${this.apiEHRBaseUrl}panel/UpadeClaimStatusHRToEHR?subdomain=${this.apiEHRSubDomain}`;
                this.commonService.httpPostWithoutHeaders(url, req, true).subscribe((response: any) => {
                    if (response.Status === 200) {
                        const actionTaken = (claimStatus === 0) ? 'rejected' : 'approved';
                        const alertDialogRef = this.matDialog.open(AlertComponent, {
                            width: '250px',
                            data: { title: 'Success', message: `The claim has been ${actionTaken}` }
                        });

                        alertDialogRef.afterClosed().subscribe((result) => {
                            console.log('The dialog was closed', result);
                            this.dialogRef.close();
                        });
                    } else {
                        this.showAlertOnAPIFailure();
                    }
                }, (err) => {
                    this.showAlertOnAPIFailure();
                    console.warn('UpadeClaimStatusHRToEHR API Failed');
                });
            }
        });
    }

    onApproveClick(): void {
        this.updateClaimStatus(1, '1');
    }

    onRejectClick(): void {
        const rejectReasonDialogRef = this.matDialog.open(ClaimRejectReasonComponent, {
            width: '450px',
            height: '350px',
            data: this.claim,
        });

        rejectReasonDialogRef.afterClosed().subscribe((result) => {
            console.warn(result);
            if (result.submitted) {
                this.dialogRef.close(result);
                this.claim.ClaimRejectReason = result.rejectReason;
                this.updateClaimStatus(0, '2');
            } else {
                console.log('The Reason Popup was closed and not submitted');
            }
        });

    }

    closePopup(): void {
        this.dialogRef.close();
    }

    showAlertOnAPIFailure(): void {
        const alertDialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Failure', message: 'Operation was unsuccessful' }
        });

        alertDialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
            this.dialogRef.close();
        });
    }

}
