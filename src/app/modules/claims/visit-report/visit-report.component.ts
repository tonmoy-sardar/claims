import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';
import { CommonFunction } from '../../../core/classes/common-function';
import { CommonService } from '../../../core/service/common.service';

@Component({
    selector     : 'visit-report',
    templateUrl  : './visit-report.component.html',
    encapsulation: ViewEncapsulation.None
})


export class VisitReportComponent implements OnInit {

    public employeeData: any = {
        dataSource: [],
        dataCount: 0,
        page: 0
    };

    startDate: any = new Date();
    endDate: any = new Date();
    maxDate: any = new Date();

    public department: any = [];
    public location: any = [];

    public searchField: any = {
        StartDate: "",
        EndDate: "",
        ClinicId: "",
        PageIndex: 1
    };

    constructor(
        private _projectService: ProjectService,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
    }

    ngOnInit() {
        let loginData = this.commonFunction.getLoginData();
        this.searchField.ClinicId = loginData.data.CompanyId;
    }

    searchEmployee() {
        let searchField: any = {
            StartDate: "",
            EndDate: "",
            ClinicId: this.searchField.ClinicId,
            PageIndex: this.searchField.PageIndex
        }

        if(this.searchField.StartDate != '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if(this.searchField.EndDate != '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        console.log("Data : ", this.searchField);
        searchField.CompanyId = this.commonService.companyId;

        this.commonService.httpViaPost('getvisitreport', searchField).subscribe((next: any) => {
            console.log("Next : ", next);
            if (next.status == 1) {
                this.employeeData.dataSource = next.response;
            }
        });
    }

    resetSort() {

    }

}
