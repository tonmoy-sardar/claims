import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { SharedModule } from 'app/shared/shared.module';

import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { NotificationAddComponent } from './notification-add/notification-add.component';
import { NotificationEditComponent } from './notification-edit/notification-edit.component';


@NgModule({
  declarations: [
    NotificationListComponent,
    NotificationDetailsComponent,
    NotificationAddComponent,
    NotificationEditComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NotificationsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class NotificationsModule { }
