import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { Observable } from 'rxjs';
import moment from 'moment';

@Component({
    selector: 'app-notification-list',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent implements OnInit {
    public pageCount = 0;
    public pageSize = 10;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public showNext;
    public showPrevious;
    public notificationData = null;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public showSelectAll = false;
    public selectAll = false;

    public companyList = [];

    public searchField: any = {
        CompanyId: '',
        NotificationType: '',
        ActiveYN: 'Y',
        IsAscending: '',
        OrderByColumnName: '',
        PageIndex: 1,
        pageNum: '',
    };

    public showLoader = true;
    constructor(
        private commonFunction: CommonFunction,
        private matDialog: MatDialog,
        private commonService: CommonService
    ) {}

    ngOnInit(): void {
        //this.sortData('VisitDate', 'desc');
        this.getCompanyList();
        this.getNotificationData();
    }

    filteraccountData(): void {
        this.pageCount = 0;
        this.getNotificationData();
    }

    getNotificationData(): void {
        this.commonService
            .httpViaPost('getnotificationdetails', this.searchField)
            .subscribe(
                (next: any) => {
                    if (next.status === 1) {
                        this.notificationData = next.response;

                        if (this.notificationData.length > 0) {
                            this.recordCount =
                                this.notificationData[0].RecordCount;
                            if (
                                this.recordCount / this.pageSize > 1 &&
                                this.pageCount * this.pageSize <
                                    this.recordCount
                            ) {
                                this.showNext = true;
                            } else {
                                this.showNext = false;
                            }
                        } else {
                            this.showNext = false;
                        }
                        this.totalPageCount = Math.ceil(
                            this.recordCount / this.pageSize
                        );
                    }
                },
                (err) => {
                    console.warn('notificationData API Failed');
                }
            );
    }

    getCompanyList(): void {
        this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyList = next.response;
            }
        }, (err) => {
            console.warn('getcompanyList API Failed.');
            this.getDummyCompanyList();
        });
    }

    resetSearch(): void {
        this.resetPageIndex = true;
        this.pageCount = 1;
        this.searchField.NotificationType = '';
        this.searchField.ActiveYN = 'Y';
        this.searchField.IsAscending = 0;
        this.searchField.OrderByColumnName = '';
        this.searchField.PageIndex = 1;
        this.searchField.pageNum = 0;

        this.getNotificationData();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getNotificationData();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageCount += 1;
        //     this.showNext = false;
        // } else {
        //     this.pageCount -= 1;
        // }
        // this.getaccountData();
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getNotificationData();
        }
    }

    selectAllRows(): void {
        this.selectAll = !this.selectAll;
        this.notificationData = [];
        if (this.selectAll) {
            this.notificationData.forEach((element) => {
                element.isChecked = true;
            });
        } else {
            this.notificationData.forEach((element) => {
                element.isChecked = false;
            });
        }
    }

    getDummyCompanyList(): void {
        this.companyList = [
            {
                "Id": 2,
                "CoName": "Hitachi",
                "Addr1": "bangi",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-01T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "John",
                "ContactOfficePhone": "018278987",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-02T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": "1",
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 3,
                "CoName": "Denso",
                "Addr1": "Bangi",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-02T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Joe",
                "ContactOfficePhone": "099787662",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-02T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10002,
                "CoName": "Tata",
                "Addr1": "Jamesdpur",
                "Addr2": "Bokaro",
                "City": "Bokaro",
                "StateId": "1",
                "EffectiveDt": "2021-08-10T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Tata",
                "ContactOfficePhone": "7890654321",
                "ContactCellPhone": "0987654321",
                "EnteredDt": "2021-08-10T00:00:00",
                "UpdatedDt": "2021-08-10T00:00:00",
                "EnteredUserId": "1",
                "UpdatedUserId": "1",
                "OHSClient": true,
                "OHSClientId": 4,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10003,
                "CoName": "Malaysian UrgentCare ",
                "Addr1": null,
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-10T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Rafee Syed",
                "ContactOfficePhone": "16159678502",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-10T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": 1,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10004,
                "CoName": "MUC Healthcare",
                "Addr1": "Kajang",
                "Addr2": "Kajang",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-25T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Gurmeet Singh",
                "ContactOfficePhone": null,
                "ContactCellPhone": "60102035283",
                "EnteredDt": "2021-10-25T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": 1,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10005,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kolkata",
                "StateId": "1",
                "EffectiveDt": null,
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10006,
                "CoName": "Company 1",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "",
                "EffectiveDt": null,
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10007,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10009,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10011,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10012,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10013,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10014,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10016,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10017,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10018,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10019,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10020,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": true,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10021,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10022,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10023,
                "CoName": "MI6",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Bond",
                "ContactOfficePhone": "",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "",
                "ContactDesignation": "Special Agent",
                "PostalCode": "",
                "ContactMailingAddress": "",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10024,
                "CoName": "wqe",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "ewrwerwe",
                "ContactOfficePhone": "",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "",
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10025,
                "CoName": "Juju",
                "Addr1": "Strret 1",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "TE",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "June",
                "ContactOfficePhone": "42874687264876",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "June@juju.com",
                "ContactDesignation": "Manager",
                "PostalCode": "454545",
                "ContactMailingAddress": "J street, Cukai",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10026,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "140",
                "StateId": "SA",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "3874843",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10027,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "30",
                "StateId": "KL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "398721897239128",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10028,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "30",
                "StateId": "KL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "27836432784823",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10029,
                "CoName": "Granger and Granger",
                "Addr1": "p",
                "Addr2": null,
                "City": "125",
                "StateId": "PJ",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Hermione G",
                "ContactOfficePhone": "743428373987298",
                "ContactCellPhone": "777777777777777",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HG@GG.com",
                "ContactDesignation": "The Office",
                "PostalCode": "36487223",
                "ContactMailingAddress": "P,P, Malaysia, 35",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10031,
                "CoName": "tt11",
                "Addr1": "HJDGEHGSD",
                "Addr2": null,
                "City": "127",
                "StateId": "PL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "RYGFEYRWGFUWEH",
                "ContactOfficePhone": "12343141442",
                "ContactCellPhone": "12314123241",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "BEFJHWRFHEW",
                "ContactDesignation": "EJFJEWHFJEW",
                "PostalCode": "EGWRGEWGW",
                "ContactMailingAddress": "JHDSBFHSBF",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10032,
                "CoName": "Potter Inc",
                "Addr1": "Street 11",
                "Addr2": null,
                "City": "77",
                "StateId": "PK",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry",
                "ContactOfficePhone": "5376357263567",
                "ContactCellPhone": "783468723636",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@PH.com",
                "ContactDesignation": "Potter",
                "PostalCode": "7867812",
                "ContactMailingAddress": "P, BS, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10033,
                "CoName": "okay",
                "Addr1": "awjdhjwa",
                "Addr2": null,
                "City": "54",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "JKHJHKHKJ",
                "ContactOfficePhone": "656756576567555",
                "ContactCellPhone": "5656565656",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "JJKHJH@KJHH.COM",
                "ContactDesignation": "JJHKHKJ",
                "PostalCode": "kjhkjsd",
                "ContactMailingAddress": "bhjghjghjhj",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10034,
                "CoName": "Dumbledore",
                "Addr1": "GH",
                "Addr2": null,
                "City": "52",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Aberforth",
                "ContactOfficePhone": "727526345263546",
                "ContactCellPhone": "3625462542",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "AB@D.com",
                "ContactDesignation": "Brother",
                "PostalCode": "42352375",
                "ContactMailingAddress": "J, M, Malaysia, 35",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10035,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kolkata",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-29T00:00:00",
                "UpdatedDt": "2021-10-29T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            }
        ];
    }

}
