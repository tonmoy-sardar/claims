import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { CommonFunction } from 'app/core/classes/common-function';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-notification-edit',
  templateUrl: './notification-edit.component.html',
  styleUrls: ['./notification-edit.component.scss']
})
export class NotificationEditComponent implements OnInit, OnDestroy  {

  constructor(private commonService: CommonService,private commonFunction: CommonFunction, private matDialog: MatDialog,private route: ActivatedRoute) { }

  public header = null;
  public body = null;
  public notificationType = 0;
  id: number;
  private sub: any;
  public companyList = null;
  CompanyId =  '';

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
   });

   this.getCompanyList();
  }

  getNotificationData(): void {

    const data: any = {
      NotificationType: '',
      ActiveYN: '',
      IsAscending: '',
      OrderByColumnName: '',
      PageIndex: 1,
      pageNum: '',
      NotificationId:this.id
  };

    this.commonService
        .httpViaPost('getnotificationdetails', data)
        .subscribe(
            (next: any) => {
                if (next.status === 1) {
                    const returnedData = next.response[0];
                    this.header = returnedData.Header;
                    this.body = returnedData.Body;
                    this.notificationType = returnedData.NotificationType;
                    this.CompanyId = returnedData.CompanyId;
                }
            },
            (err) => {
                console.warn('notificationData API Failed');
            }
        );
}

  submitNotification(): void {
    let loggedInUser: any = this.commonFunction.getLoginData().data;
    const data = {
      "NotificationId" : this.id,
      "Header": this.header,
      "Body": this.body,
      "NotificationType": this.notificationType,
      "ActiveYN": 'N',
      "LogedinUser":loggedInUser.UserName,
      "CompanyId":this.CompanyId
      };

    this.commonService.httpViaPost('addupdatenotification ', data).subscribe((next: any) => {
      if (next.status == 1) {

        const dialogRef = this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Success', message: 'Notification Edited Successfully' }
        });
        
      }
    });
  }

  getCompanyList(): void {
    this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
        if (next.status === 1) {
            this.companyList = next.response;
            this.getNotificationData();
        }
    }, (err) => {
        console.warn('getcompanyList API Failed.');
    });
}

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
