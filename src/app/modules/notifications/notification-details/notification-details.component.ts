import { Component, OnInit } from '@angular/core';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { CommonFunction } from 'app/core/classes/common-function';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.scss']
})
export class NotificationDetailsComponent implements OnInit {

  constructor(private commonService: CommonService,private commonFunction: CommonFunction, private matDialog: MatDialog,private route: ActivatedRoute) { }

  public header = null;
  public body = null;
  public notificationType = 0;
  id: number;
  private sub: any;
  public companyList: any;
  public CompanyId = '';

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
   });

   this.getCompanyList();
  }

  getNotificationData(): void {

    const data: any = {
      NotificationType: '',
      ActiveYN: '',
      IsAscending: '',
      OrderByColumnName: '',
      PageIndex: 1,
      pageNum: '',
      NotificationId:this.id
  };

    this.commonService
        .httpViaPost('getnotificationdetails', data)
        .subscribe(
            (next: any) => {
                if (next.status === 1) {
                    const returnedData = next.response[0];
                    this.header = returnedData.Header;
                    this.body = returnedData.Body;
                    this.CompanyId = returnedData.CompanyId;
                    this.notificationType = returnedData.NotificationType;
                }
            },
            (err) => {
                console.warn('notificationData API Failed');
            }
        );
}


getCompanyList(): void {
  this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
      if (next.status === 1) {
          this.companyList = next.response;
          this.getNotificationData();
      }
  }, (err) => {
      console.warn('getcompanyList API Failed.');
  });
}


}
