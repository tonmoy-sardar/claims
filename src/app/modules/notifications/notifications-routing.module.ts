import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotificationAddComponent } from './notification-add/notification-add.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { NotificationEditComponent } from './notification-edit/notification-edit.component';
import { NotificationListComponent } from './notification-list/notification-list.component';

const routes: Routes = [
  { path: '', component: NotificationListComponent },
  { path: 'details/:id', component: NotificationDetailsComponent },
  { path: 'add', component: NotificationAddComponent },
  { path: 'edit/:id', component: NotificationEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
