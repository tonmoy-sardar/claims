import { Component, OnInit } from '@angular/core';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { CommonFunction } from 'app/core/classes/common-function';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-notification-add',
  templateUrl: './notification-add.component.html',
  styleUrls: ['./notification-add.component.scss']
})
export class NotificationAddComponent implements OnInit {

  constructor(private commonService: CommonService,private commonFunction: CommonFunction, private matDialog: MatDialog,) { }

  public header = null;
  public body = null;
  public notificationType = 0;
  public companyList = null;
  public CompanyId = '';

  ngOnInit(): void {
    this.getCompanyList();
  }

  submitNotification(): void {
    let loggedInUser: any = this.commonFunction.getLoginData().data;
    const data = {
      "NotificationId" : "0",
      "Header": this.header,
      "Body": this.body,
      "NotificationType": this.notificationType,
      "ActiveYN": 'N',
      "LogedinUser":loggedInUser.UserName,
      "CompanyId":this.CompanyId
      };

    this.commonService.httpViaPost('addupdatenotification ', data).subscribe((next: any) => {
      if (next.status == 1) {

        const dialogRef = this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Success', message: 'Notification Created Successfully' }
        });
        
      }
    });
  }

  getCompanyList(): void {
    this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
        if (next.status === 1) {
            this.companyList = next.response;
        }
    }, (err) => {
        console.warn('getcompanyList API Failed.');
    });
}

}
