import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountPayableComponent } from './account-payable/account-payable.component';
import { AccountReceivableComponent } from './account-receivable/account-receivable.component';


const routes: Routes = [
    { path: 'receivable', component: AccountReceivableComponent },
    { path: 'payable', component: AccountPayableComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AccountRoutingModule { }
