import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { SharedModule } from 'app/shared/shared.module';
import { AccountPayableComponent } from './account-payable/account-payable.component';
import { AccountReceivableComponent } from './account-receivable/account-receivable.component';
import { AccountRoutingModule } from './account-routing.module';


@NgModule({
    declarations: [
        AccountReceivableComponent,
        AccountPayableComponent
    ],
    imports: [
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AccountRoutingModule,
        SharedModule
    ]
})

export class AccountModule { }
