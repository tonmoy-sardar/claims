import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import moment from 'moment';
import { Observable } from 'rxjs';


@Component({
    selector: 'account-payable',
    styleUrls: ['account-payable.component.scss'],
    templateUrl: './account-payable.component.html'
})


export class AccountPayableComponent implements OnInit {
    public pageCount = 1;
    public showNext;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public accountData = [];
    public clinicData: any;
    public showGenerateInvoiceButton = false;
    public generateInvoiceList = [];
    public showSelectAll = false;
    public selectAll = false;
    startDate: any = new Date();
    endDate: any = new Date();
    public companyList = [];
    public min_date = null;

    public searchField: any = {
        // StartDate: '',
        // EndDate: '',
        StartDate: moment().startOf('M'),
        EndDate: moment(),
        ClinicCode: "",
        CompanyName: "",
        CompanyId:"",
        IsAscending: '0',
        OrderByColumnName: '',
        ClaimsType: '',
        ClaimStatus: 'Approve',
        IsPayableInvoiceGenerated: '',
        PageIndex: '1'
    };

    public showLoader = true;
    constructor(private commonFunction: CommonFunction,
        private matDialog: MatDialog,
        private commonService: CommonService) {
    }


    ngOnInit(): void {
        this.getClinic();
        this.getCompanyList();
        this.sortData('VisitDate', 'desc');
        this.min_date = new Date(2020, 0, 1);
    }

    filteraccountData(): void {
        this.pageCount = 1;
        this.getaccountData();
    }

    getaccountData(): void {
        this.showLoader = true;
        this.searchField.PageIndex = this.pageCount;
        this.requestaccountData().subscribe((data: any) => {
            if (data.status === 1) {
                this.showLoader = false;
                this.accountData = data.response;
                let counter = 0;
                if (this.accountData.length > 0) {
                    this.accountData.forEach(element => {
                        element.isChecked = false;
                        if (element.IsPayableInvoiceGenerated === 'Y') {
                            counter += 1;
                        }
                    });
                    if (counter == this.accountData.length) {
                        this.showSelectAll = false;
                    } else {
                        this.showSelectAll = true;
                    }

                    this.recordCount = this.accountData[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('getmcreport API failed');
        });
    }

    requestaccountData(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);

        if (this.searchField.StartDate && this.searchField.StartDate !== '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if (this.searchField.EndDate && this.searchField.EndDate !== '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        // searchField.CompanyId = this.commonService.companyId;
        searchField.LoginUserId = this.commonService.loggedInUserId;

        return this.commonService.httpViaPost('getclaimsdetsils', searchField);
    }


    getClinic(): void {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            console.log('Next : ', next);
            if (next.status === 1) {
                this.clinicData = next.response;
                console.log('this.clinicData : ', this.clinicData);
            }
        }, (err) => {
            console.warn('getclinicname API failed');
            this.getDummyClinicData();
        });
    }

    
    getCompanyList(): void {
        this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyList = next.response;
            }
        }, (err) => {
            console.warn('getcompanyList API Failed.');
            this.getDummyCompanyList();
        });
    }

    getDummyCompanyList(): void {
        this.companyList = [
            {
                "Id": 2,
                "CoName": "Hitachi",
                "Addr1": "bangi",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-01T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "John",
                "ContactOfficePhone": "018278987",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-02T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": "1",
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 3,
                "CoName": "Denso",
                "Addr1": "Bangi",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-02T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Joe",
                "ContactOfficePhone": "099787662",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-02T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10002,
                "CoName": "Tata",
                "Addr1": "Jamesdpur",
                "Addr2": "Bokaro",
                "City": "Bokaro",
                "StateId": "1",
                "EffectiveDt": "2021-08-10T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Tata",
                "ContactOfficePhone": "7890654321",
                "ContactCellPhone": "0987654321",
                "EnteredDt": "2021-08-10T00:00:00",
                "UpdatedDt": "2021-08-10T00:00:00",
                "EnteredUserId": "1",
                "UpdatedUserId": "1",
                "OHSClient": true,
                "OHSClientId": 4,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10003,
                "CoName": "Malaysian UrgentCare ",
                "Addr1": null,
                "Addr2": null,
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-08-10T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Rafee Syed",
                "ContactOfficePhone": "16159678502",
                "ContactCellPhone": null,
                "EnteredDt": "2021-08-10T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": 1,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10004,
                "CoName": "MUC Healthcare",
                "Addr1": "Kajang",
                "Addr2": "Kajang",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-25T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": "Gurmeet Singh",
                "ContactOfficePhone": null,
                "ContactCellPhone": "60102035283",
                "EnteredDt": "2021-10-25T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": "1",
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": 1,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10005,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kolkata",
                "StateId": "1",
                "EffectiveDt": null,
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10006,
                "CoName": "Company 1",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "",
                "EffectiveDt": null,
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10007,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10009,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": true,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10011,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10012,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": null,
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10013,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": null,
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10014,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10016,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10017,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10018,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": null,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10019,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": null,
                "ContactName": null,
                "ContactOfficePhone": null,
                "ContactCellPhone": null,
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": true,
                "OHSClientId": null,
                "sihatkuPanelCompCode": null,
                "Email": null,
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10020,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": true,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10021,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10022,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kajang",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10023,
                "CoName": "MI6",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Bond",
                "ContactOfficePhone": "",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "",
                "ContactDesignation": "Special Agent",
                "PostalCode": "",
                "ContactMailingAddress": "",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10024,
                "CoName": "wqe",
                "Addr1": "",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "ewrwerwe",
                "ContactOfficePhone": "",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "",
                "ContactDesignation": null,
                "PostalCode": null,
                "ContactMailingAddress": null,
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10025,
                "CoName": "Juju",
                "Addr1": "Strret 1",
                "Addr2": null,
                "City": "Kajang",
                "StateId": "TE",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "June",
                "ContactOfficePhone": "42874687264876",
                "ContactCellPhone": "",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "June@juju.com",
                "ContactDesignation": "Manager",
                "PostalCode": "454545",
                "ContactMailingAddress": "J street, Cukai",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10026,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "140",
                "StateId": "SA",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "3874843",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10027,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "30",
                "StateId": "KL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "398721897239128",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10028,
                "CoName": "Potter Inc",
                "Addr1": "Street 1",
                "Addr2": null,
                "City": "30",
                "StateId": "KL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry Potter",
                "ContactOfficePhone": "474623746236",
                "ContactCellPhone": "27836432784823",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@HP.com",
                "ContactDesignation": "Auror",
                "PostalCode": "7386837",
                "ContactMailingAddress": "KL, KL, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10029,
                "CoName": "Granger and Granger",
                "Addr1": "p",
                "Addr2": null,
                "City": "125",
                "StateId": "PJ",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Hermione G",
                "ContactOfficePhone": "743428373987298",
                "ContactCellPhone": "777777777777777",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HG@GG.com",
                "ContactDesignation": "The Office",
                "PostalCode": "36487223",
                "ContactMailingAddress": "P,P, Malaysia, 35",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10031,
                "CoName": "tt11",
                "Addr1": "HJDGEHGSD",
                "Addr2": null,
                "City": "127",
                "StateId": "PL",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "RYGFEYRWGFUWEH",
                "ContactOfficePhone": "12343141442",
                "ContactCellPhone": "12314123241",
                "EnteredDt": "2021-10-27T00:00:00",
                "UpdatedDt": "2021-10-27T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "BEFJHWRFHEW",
                "ContactDesignation": "EJFJEWHFJEW",
                "PostalCode": "EGWRGEWGW",
                "ContactMailingAddress": "JHDSBFHSBF",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10032,
                "CoName": "Potter Inc",
                "Addr1": "Street 11",
                "Addr2": null,
                "City": "77",
                "StateId": "PK",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Harry",
                "ContactOfficePhone": "5376357263567",
                "ContactCellPhone": "783468723636",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "HP@PH.com",
                "ContactDesignation": "Potter",
                "PostalCode": "7867812",
                "ContactMailingAddress": "P, BS, Malaysia",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10033,
                "CoName": "okay",
                "Addr1": "awjdhjwa",
                "Addr2": null,
                "City": "54",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "JKHJHKHKJ",
                "ContactOfficePhone": "656756576567555",
                "ContactCellPhone": "5656565656",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "JJKHJH@KJHH.COM",
                "ContactDesignation": "JJHKHKJ",
                "PostalCode": "kjhkjsd",
                "ContactMailingAddress": "bhjghjghjhj",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10034,
                "CoName": "Dumbledore",
                "Addr1": "GH",
                "Addr2": null,
                "City": "52",
                "StateId": "JH",
                "EffectiveDt": null,
                "Active": false,
                "ExpirationDt": null,
                "ContactName": "Aberforth",
                "ContactOfficePhone": "727526345263546",
                "ContactCellPhone": "3625462542",
                "EnteredDt": "2021-10-28T00:00:00",
                "UpdatedDt": "2021-10-28T00:00:00",
                "EnteredUserId": null,
                "UpdatedUserId": null,
                "OHSClient": false,
                "OHSClientId": 0,
                "sihatkuPanelCompCode": 0,
                "Email": "AB@D.com",
                "ContactDesignation": "Brother",
                "PostalCode": "42352375",
                "ContactMailingAddress": "J, M, Malaysia, 35",
                "IsActiveYN": null,
                "InactiveDate": null
            },
            {
                "Id": 10035,
                "CoName": "ABCD pvt",
                "Addr1": "Kolkata",
                "Addr2": "Kolkata",
                "City": "Kolkata",
                "StateId": "1",
                "EffectiveDt": "2021-10-26T00:00:00",
                "Active": false,
                "ExpirationDt": "2021-10-26T00:00:00",
                "ContactName": "Ram Sharma",
                "ContactOfficePhone": "9999999999",
                "ContactCellPhone": "9999999999",
                "EnteredDt": "2021-10-29T00:00:00",
                "UpdatedDt": "2021-10-29T00:00:00",
                "EnteredUserId": "10",
                "UpdatedUserId": "11",
                "OHSClient": false,
                "OHSClientId": 12,
                "sihatkuPanelCompCode": 12,
                "Email": "abhishek@gmail.com",
                "ContactDesignation": "Developer",
                "PostalCode": "123",
                "ContactMailingAddress": "Kolkata 150",
                "IsActiveYN": null,
                "InactiveDate": null
            }
        ];
    }

    resetSearch(): void {
        this.resetPageIndex = true;

        this.selectAll = false;
        this.pageCount = 1;
        // this.searchField.StartDate = ''
        // this.searchField.EndDate = '';
        this.searchField.StartDate = moment().startOf('M');
        this.searchField.EndDate = moment();
        this.searchField.VisitDate = '';
        this.searchField.CompanyName = '';
        this.searchField.CompanyId = '';
        this.searchField.ClinicCode = '';
        this.searchField.ClinicId = '';
        this.searchField.ClaimsType = '';
        this.searchField.EmployeeName = '';
        this.searchField.IsPayableInvoiceGenerated = '';
        this.searchField.OrderByColumnName = 'VisitDate';
        this.searchField.ClaimStatus = 'Approve';
        this.searchField.IsAscending = 0;
        this.searchField.PageIndex = 1;

        this.getaccountData();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getaccountData();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageCount += 1;
        //     this.showNext = false;
        // } else {
        //     this.pageCount -= 1;
        // }
        // this.getaccountData();
        
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getaccountData();
        }
    }


    checkValue(element: any, event: any) {
        const selectCheckBox = event.target.checked;
        element.isChecked = selectCheckBox;
        if (selectCheckBox) {
            this.showGenerateInvoiceButton = true;
            this.generateInvoiceList.push(element.Id);
            if (this.generateInvoiceList.length === this.accountData.length) {
                this.selectAll = true;
            }
        } else {
            this.selectAll = false;
            const index = this.generateInvoiceList.findIndex(x => x === element.Id);
            if (index > -1) {
                this.generateInvoiceList.splice(index, 1);
            }
            if (this.generateInvoiceList.length > 0) {
                this.showGenerateInvoiceButton = true;
            } else {
                this.showGenerateInvoiceButton = false;
            }
        }
        console.log(JSON.stringify(this.generateInvoiceList));
        console.log((this.generateInvoiceList.toString()));
    }

    requestInvoiceGeneration(): any {
        const searchRequest = {
            'ClaimsId': this.generateInvoiceList.toString(),
            'CompanyId': this.commonService.companyId
        };
        return this.commonService.httpViaPost('accountpaybleinvoicegeneration', searchRequest)
            .subscribe((data) => {
                if (data.message === 'Successful') {
                    const alertDialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: 'Invoice generation is successful' }
                    });

                    alertDialogRef.afterClosed().subscribe((result) => {
                        this.filteraccountData();
                    });

                } else {
                    console.warn('accountpaybleinvoicegeneration API is not working');
                    const alertDialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Failure', message: 'Invoice generation Failed' }
                    });
                }
                console.warn(data);
            });
    }

    selectAllRows(): void {
        this.selectAll = !this.selectAll;
        this.generateInvoiceList = [];
        if (this.selectAll) {
            this.showGenerateInvoiceButton = true;
            this.accountData.forEach(element => {
                if (element.IsPayableInvoiceGenerated === 'N') {
                    element.isChecked = true;
                    this.generateInvoiceList.push(element.Id);
                }
            });
        } else {
            this.showGenerateInvoiceButton = false;
            this.accountData.forEach(element => {
                if (element.IsPayableInvoiceGenerated === 'N') {
                    element.isChecked = false;
                }
            });
        }
        console.log('generateInvoiceList: ' + (this.generateInvoiceList.toString()));
    }

    changeEndDate(): any {
        if(this.searchField.StartDate == ''){
          return this.min_date
        }else{
          return this.searchField.StartDate;
        }
      }

    getDummyClinicData(): void {
        this.clinicData = [
            {
                'clinicName': 'D02'
            },
            {
                'clinicName': 'Tst001'
            },
            {
                'clinicName': 'Tst002'
            }
        ];
    }

}
