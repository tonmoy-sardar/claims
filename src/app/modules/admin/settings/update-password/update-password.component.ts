import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { FuseAlertType } from '@fuse/components/alert';
import { FuseValidators } from '@fuse/validators';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  @ViewChild('resetPasswordNgForm') resetPasswordNgForm: NgForm;

  public resetPasswordForm: FormGroup;
  public showAlert: boolean = false;
  private alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(
    private _formBuilder: FormBuilder,
    private commonService: CommonService,
    private commonFunction: CommonFunction,
    private matDialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.resetPasswordForm = this._formBuilder.group({
      oldPassword: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
      passwordConfirm: ['', Validators.required]
    },
      {
        validators: FuseValidators.mustMatch('password', 'passwordConfirm')
      }
    );
  }

  resetPassword(): void {
    if (this.resetPasswordForm.invalid) {
      return;
    }
    this.resetPasswordForm.disable();
    this.showAlert = false;

    const request = {
      'NewPassword': this.resetPasswordForm.get('password').value.toString(),
      'oldPassword': this.resetPasswordForm.get('oldPassword').value.toString(),
      'UserName': this.commonFunction.getLoginData().data.UserName
    };
    this.commonService.httpViaPost('UpdatePassword', request)
      .pipe(
        finalize(() => {
          this.resetPasswordForm.enable();
          this.resetPasswordNgForm.resetForm()
          this.showAlert = true;
        })
      )
      .subscribe((next) => {
        if (next.status === 1) {
          const dialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Success', message: 'Your password has been reset.' }
          });

          dialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
          });
        }
      }, (error) => {
        this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Failure', message: 'Something went wrong, please try again.' }
        });
      }
      );
  }

}
