import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from 'app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { FuseAlertModule } from '@fuse/components/alert';

const routes: Routes = [
  { path: '', component: UpdatePasswordComponent }
];

@NgModule({
  declarations: [
    UpdatePasswordComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FuseAlertModule,
    CommonModule
  ]
})
export class SettingsModule { }
