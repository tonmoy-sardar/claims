import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { SharedModule } from '../../../core/modules/shared.module';

import { SalesSummaryComponent } from './sales-summary/sales-summary.component'; 
import { AbnormalExamResultsComponent } from './abnormal-exam-results/abnormal-exam-results.component';
import { AudiometricReportComponent } from './audiometric-report/audiometric-report.component';
import { MedicalSurveillanceSummaryComponent } from './medical-surveillance-summary/medical-surveillance-summary.component';
import { EmptyOhsComponent } from './empty-ohs/empty-ohs.component';

@NgModule({
  declarations: [
    SalesSummaryComponent, 
    AbnormalExamResultsComponent, 
    AudiometricReportComponent, 
    MedicalSurveillanceSummaryComponent,
    EmptyOhsComponent
  ],
  imports: [
    CommonModule,
    ReportRoutingModule,
    SharedModule
  ]
})
export class ReportModule { }
