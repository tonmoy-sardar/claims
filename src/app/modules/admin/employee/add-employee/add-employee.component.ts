import { AfterViewInit, Component, OnInit, Optional, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../../../core/service/common.service';
import { CommonFunction } from '../../../../core/classes/common-function';
import * as XLSX from 'xlsx';
import { toLower } from 'lodash';
import { Router, RouterModule } from '@angular/router';
import { AlertComponent } from '../../../../core/component/alert/alert.component';
import { NullTemplateVisitor } from '@angular/compiler';
import * as moment from 'moment';




@Component({
    selector: 'add-employee',
    templateUrl: './add-employee.component.html',
    styleUrls: ['add-employee.css'],
    encapsulation: ViewEncapsulation.None
})

export class AddEmployeeComponent implements OnInit {
    public employeeData: any = {
        displayedColumns: [],
        dataSource: [],
        totalCount: 0,
        errorCount: 0,

        allData: [],
        allDataColumns: [],
        page: 0,
        totalData: 0
    };
    public department: any = [];
    public location: any = [];

    public sortField: any = {
        "SortColumn": "",
        "SortOrder": "",
        "PageIndex": 1,
        "CompanyId": "",
        "name": "",
        "Gender": "",
        "Retired": '',
        "Department": "",
        "Nationality": ""
    };
    public sortOrderVal: string;
    public sortedColumn: string;
    constructor(
        private _bottomSheet: MatBottomSheet,
        public dialog: MatDialog,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnInit() {
        let loginUserData: any = this.commonFunction.getLoginData();
        this.commonService.httpViaPost('cmpdepartmentlist', { "CompanyId": loginUserData.data.CompanyId }).subscribe((next: any) => {
            if (next.status == 1) {
                this.department = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.nationality = next.response;
            }
        });

        // this.nextPage('next');
        this.sortOrder("", "");
    }

    nextPage(str: string) {
        if (str == 'next') {
            this.employeeData.page++;
        } else {
            this.employeeData.page--;
        }

        let loginUserData: any = this.commonFunction.getLoginData();
        let data: any = {
            "PageIndex": this.employeeData.page,
            "CompanyId": loginUserData.data.CompanyId
        };
        this.commonService.httpViaPost('getemployeelist', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.employeeData.allData = next.response;

                if (this.employeeData.allData.length > 0) {
                    this.employeeData.allDataColumns = Object.keys(this.employeeData.allData[0]);
                    this.employeeData.totalData = this.employeeData.allData[0].RecordCount;
                }
            }
        });
    }

    sortOrder(sort_column: string, order: string) {
        this.employeeData.page = 1;
        this.sortOrderVal = order;
        this.sortedColumn = sort_column;

        let loginUserData: any = this.commonFunction.getLoginData();
        let data: any = {
            "SortColumn": sort_column,
            "SortOrder": order,
            "PageIndex": this.employeeData.page,
            "CompanyId": loginUserData.data.CompanyId,
            "name": "",
            "Gender": this.sortField.Gender,
            "Department": this.sortField.Department,
            "Nationality": this.sortField.Nationality
        };
        this.commonService.httpViaPost('empfilter', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.employeeData.allData = next.response;

                if (this.employeeData.allData.length > 0) {
                    this.employeeData.allDataColumns = Object.keys(this.employeeData.allData[0]);
                    this.employeeData.totalData = this.employeeData.allData[0].RecordCount;
                }
            }
        });
    }

    filterEmployee() {
        this.employeeData.page = 1;

        let loginUserData: any = this.commonFunction.getLoginData();
        this.sortField.CompanyId = loginUserData.data.CompanyId;
        let data: any = this.sortField;
        this.commonService.httpViaPost('empfilter', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.employeeData.allData = next.response;

                if (this.employeeData.allData.length > 0) {
                    this.employeeData.allDataColumns = Object.keys(this.employeeData.allData[0]);
                    this.employeeData.totalData = this.employeeData.allData[0].RecordCount;
                }
            }
        });
    }

    resetSort() {
        this.sortField.SortColumn = "";
        this.sortField.SortOrder = "";
        this.sortField.PageIndex = 1;
        this.sortField.CompanyId = "";
        this.sortField.name = "";
        this.sortField.Gender = "";
        this.sortField.Department = "";
        this.sortField.Nationality = "";

        this.filterEmployee();
    }

    export() {
        let empDetails = [];
        let errorItem = [];

        // get not error data
        for (let loop = 0; loop < this.employeeData.dataSource.length; loop++) {
            if (this.employeeData.dataSource[loop].error == false) {
                this.employeeData.dataSource[loop].date_of_birth = "02/12/1990";
                this.employeeData.dataSource[loop].dateofhire = "02/12/1990";

                if (this.employeeData.dataSource[loop].gender.toLowerCase() == 'female') {
                    this.employeeData.dataSource[loop].gender = 'F';
                } else {
                    this.employeeData.dataSource[loop].gender = 'M';
                }

                empDetails.push(this.employeeData.dataSource[loop]);
            } else {
                this.employeeData.dataSource[loop].date_of_birth = "02/12/1990";
                this.employeeData.dataSource[loop].dateofhire = "02/12/1990";
                errorItem.push(this.employeeData.dataSource[loop]);
            }
        }

        // setup for
        let loginUserData: any = this.commonFunction.getLoginData();
        let data: any = {
            EmployeeDetails: empDetails,
            "CompanyId": loginUserData.data.CompanyId
        };

        this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.employeeData.dataSource = [];
                for (let loop = 0; loop < errorItem.length; loop++) {
                    this.employeeData.dataSource.push(errorItem[loop]);
                }
                this.employeeData.totalCount = this.employeeData.errorCount;
                this.employeeData.errorCount = this.employeeData.errorCount;

                if (this.employeeData.errorCount === 0) {

                }
                let d = confirm("Successfully export.");

                if (d) {
                    location.reload();
                } else {
                    location.reload();
                }
            }
        });
    }

    selectFile(event) {
        let file = event.target.files[0];
        let fileReader = new FileReader();
        fileReader.readAsArrayBuffer(file);
        fileReader.onload = (e) => {
            let arrayBuffer: any = fileReader.result;
            var data = new Uint8Array(arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];

            // Get array
            var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: false, defval: null });

            // change object key
            let newJsonData: any = [];
            arraylist.forEach(element => {
                newJsonData.push(this.convertKeysToLowerCase(element));
            });

            // getting error
            newJsonData.forEach(element => {
                if (element.name == "" || element.passport_no_ic == "" || element.name == null || element.passport_no_ic == null) {
                    element.error = true;
                    this.employeeData.errorCount++;
                } else {
                    element.error = false;
                }

                // for gender
                let gender = element.gender.toLowerCase();
                switch (gender[0]) {
                    case 'm':
                        element.gender = 'male';
                        break;
                    case 'f':
                        element.gender = 'female';
                        break;
                    default:
                        element.gender = 'others';
                        break;
                }

                // for date of birth
                // element.gender.date_of_birth = new Date();
            });

            // get col name
            this.employeeData.dataSource = newJsonData.sort(function (a, b) {
                if (a.error == true) {
                    return -1;
                } else {
                    return 0;
                }
            });
            this.employeeData.totalCount = newJsonData.length;

            if (this.employeeData.dataSource.length > 0) {
                this.employeeData.displayedColumns = Object.keys(arraylist[0]);
            }
            this.employeeData.displayedColumns.push('Error');
        }
    }

    convertKeysToLowerCase(obj) {
        var output = {};
        for (let i in obj) {
            if (Object.prototype.toString.apply(obj[i]) === '[object Object]') {
                output[i.toLowerCase().replace(/\s/g, "_").replace(/\//g, '_')] = this.convertKeysToLowerCase(obj[i]);
            } else if (Object.prototype.toString.apply(obj[i]) === '[object Array]') {
                output[i.toLowerCase().replace(/\s/g, "_").replace(/\//g, '_')] = [];
                output[i.toLowerCase().replace(/\s/g, "_").replace(/\//g, '_')].push(this.convertKeysToLowerCase(obj[i][0]));
            } else {
                output[i.toLowerCase().replace(/\s/g, "_").replace(/\//g, '_')] = obj[i];
            }
        }
        return output;
    }

    viewData(index) {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = this.employeeData.dataSource[index];

        const dialogRef = this.dialog.open(DialogContentExampleDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            this.employeeData.dataSource[index] = {};
            this.employeeData.dataSource[index] = result.data;

            if (this.employeeData.dataSource[index].error == true && result.data.error == false) {
                this.employeeData.errorCount--;
            }
        });
    }

    editEmployee(index: any) {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = this.employeeData.allData[index];

        const dialogRef = this.dialog.open(EditEmployee, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            console.log("Hello World");
            this.employeeData.page--;
            // this.nextPage('next');
            this.sortOrder("", "");
        });
    }

    public sortEmployeeForm = this.formBuilder.group({
        name: ['', []],
        department: ['', []],
        nationality: ['', []],
        gender: ['', []]
    });

}



@Component({
    selector: 'dialog-content-example-dialog',
    templateUrl: 'dialog-content-example-dialog.html',
})

export class DialogContentExampleDialog implements OnInit {

    displayedColumns: string[] = [];
    dataSource = [];
    department: any = [];
    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        doh: ['', []],
        department: ['', []],
        kwsp: ['', []],
        ic_passport: ['', [Validators.required]],
        nationality: ['', []],
        telNo: ['', []],
        dob: ['', []],
        gender: ['', []],
        address_line_1: ['', []],
        address_line_2: ['', []],
        city: ['', []],
        state: ['', []],
        postCode: ['', []],
        marticalStatus: ['single', []],
        noOfChildren: [0, []],
    });

    constructor(
        public dialogRef: MatDialogRef<DialogContentExampleDialog>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
    }

    ngOnInit() {
    }

    addEmployeeFormSubmit() {
        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            this.data.error = false;
            this.dialogRef.close({ event: 'close', data: this.data });
        }
    }

    closeModal() {
        this.dialogRef.close({ event: 'close', data: this.data });
    }

}






@Component({
    selector: 'edit-employee',
    templateUrl: 'edit-employee.html',
})

export class EditEmployee implements OnInit {

    location = {
        state: [],
        city: [],
        nationality: []
    };
    dob_range: any = new Date();
    doh_range: any = new Date();
    department: any = [];
    isChecked: boolean = false;
    isResignedChecked: boolean = false;

    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.maxLength(50)]],
        Employee_No: ['', []],
        dateofhire: ['', []],
        department: ['', []],
        kwsp: ['', []],
        passport_no_ic: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        nationality: ['', [Validators.maxLength(2)]],
        phone: ['', [Validators.maxLength(15), Validators.pattern("^[0-9\-\/]*$")]],
        date_of_birth: ['', []],
        gender: ['', [Validators.maxLength(1)]],
        address_line_1: ['', [Validators.maxLength(100)]],
        address_line_2: ['', [Validators.maxLength(100)]],
        city: ['', [Validators.maxLength(100)]],
        state: ['', [Validators.maxLength(100)]],
        postCode: ['', [Validators.maxLength(100)]],
        marticalStatus: ['', [Validators.maxLength(10)]],
        noOfChildren: ['', []],
        retired: [false, []],
        retired_date: ['', []],
        resigned_date: ['', []]
    });

    constructor(
        public dialogRef: MatDialogRef<EditEmployee>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private matDialog: MatDialog,
        private router: Router
    ) {
    }

    closeModal() {
        this.dialogRef.close({ event: 'close', data: this.data });
    }

    checkValue(event: any) {
        console.log(event);
    }

    ngOnInit(): void {
        // get department
        let loginUserData: any = this.commonFunction.getLoginData();
        this.commonService.httpViaPost('cmpdepartmentlist', { "CompanyId": loginUserData.data.CompanyId }).subscribe((next: any) => {
            if (next.status == 1) {
                this.department = next.response;
            }
        });

        // get state
        this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.state = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.nationality = next.response;
            }
        });


        this.dob_range.setMonth(this.dob_range.getMonth() - 120);
        this.doh_range.setMonth(this.doh_range.getMonth() + 1);

        console.log("this.data : ", this.data);

        this.data.Date_Of_Birth = moment(this.data.Date_Of_Birth);
        this.data.DateOfHire = moment(this.data.DateOfHire);



        this.addEmployeeForm.patchValue({
            name: this.data.EmpName,
            Employee_No: this.data.EmpNo,
            dateofhire: this.data.DateOfHire,
            department: this.data.Department,
            kwsp: this.data.KWSP,
            passport_no_ic: this.data.ICNum,
            nationality: this.data.Nationality,
            phone: this.data.Phone,
            date_of_birth: this.data.Date_Of_Birth,
            gender: this.data.Gender,
            address_line_1: this.data.Addr1,
            address_line_2: this.data.Addr2,
            postCode: this.data.ZipCode,
            marticalStatus: this.data.marriedStatus,
            noOfChildren: this.data.noOfChildren,
            state: this.data.StateId
        });

        let d = {
            target: {
                value: this.data.StateId
            }
        };
        this.changeState(d);
    }

    changeState(event) {
        let data: any = {
            "StateListId": event.target.value,
            'CompanyId': this.commonService.companyId
        };

        this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.city = next.response;

                this.addEmployeeForm.patchValue({
                    city: this.data.City
                });
            }
        });
    }

    keyPressIcPassport(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    keyPressPhoneNumber(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[0-9\-\/]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    addEmployeeFormSubmit() {
        console.log("this.addEmployeeForm : ", this.addEmployeeForm.value);

        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            if (this.addEmployeeForm.value.dateofhire != "") {
                this.addEmployeeForm.value.dateofhire = this.addEmployeeForm.value.dateofhire.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.date_of_birth != "") {
                this.addEmployeeForm.value.date_of_birth = this.addEmployeeForm.value.date_of_birth.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.phone) {
                let phone = this.addEmployeeForm.value.phone;
                phone.replace('-', '');
                phone = parseInt(phone);
                if (phone > 999999) {
                    this.addEmployeeForm.value.phone = "";
                }
            }


            let arr: any = [this.addEmployeeForm.value];

            let loginUserData: any = this.commonFunction.getLoginData();
            let data: any = {
                EmployeeDetails: arr,
                CompanyId: loginUserData.data.CompanyId,
                userId: loginUserData.data.Id
            };

            this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
                if (next.status == 1) {
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { name: "", animal: "" }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        this.closeModal();
                    });
                }
            });
        } else {
            console.log("this.addEmployeeForm not valid.");
        }
    }

}




