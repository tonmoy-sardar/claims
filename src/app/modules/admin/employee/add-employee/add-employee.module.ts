import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AddEmployeeComponent, EditEmployee, DialogContentExampleDialog } from 'app/modules/admin/employee/add-employee/add-employee.component';
import { MaterialModule } from '../../../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from '../../../../core/service/common.service';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../../../my-date-formats';





const exampleRoutes: Route[] = [
    {
        path: '',
        component: AddEmployeeComponent
    }
];

@NgModule({
    declarations: [
        AddEmployeeComponent,
        EditEmployee,
        DialogContentExampleDialog
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(exampleRoutes)
    ],
    entryComponents: [
        DialogContentExampleDialog,
        EditEmployee
    ],
    providers: [
        CommonService,
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class AddEmployeeModule {
}
