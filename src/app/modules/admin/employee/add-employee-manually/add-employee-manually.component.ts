import { AfterViewInit, Component, Optional, Inject, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import * as XLSX from 'xlsx';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { CommonService } from '../../../../core/service/common.service';
import { CommonFunction } from '../../../../core/classes/common-function';
import * as moment from 'moment';
import { AlertComponent } from '../../../../core/component/alert/alert.component';
import { MatDialog } from '@angular/material/dialog';




@Component({
    selector: 'add-employee-manually',
    templateUrl: './add-employee-manually.component.html',
    styleUrls: ['add-employee-manually.css'],
    encapsulation: ViewEncapsulation.None
})

export class AddEmployeeManuallyComponent implements OnInit {

    displayedColumns: string[] = [];
    dataSource = [];
    location = {
        state: [],
        city: [],
        nationality: []
    };
    department = [];
    costCenter = [];
    dob_range: any = new Date();
    doh_range: any = new Date();
    retired_date: any = new Date();
    resigned_date: any = new Date();
    isChecked: boolean = false;
    isResignedChecked: boolean = false;

    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.maxLength(50)]],
        Employee_No: ['', []],
        dateofhire: ['', []],
        department: ['', []],
        costCenter: ['', []],
        kwsp: ['', []],
        passport_no_ic: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        nationality: ['', [Validators.maxLength(2)]],
        phone: ['', [Validators.maxLength(15), Validators.pattern("^[0-9\-\/]*$")]],
        date_of_birth: ['', []],
        gender: ['', [Validators.maxLength(1)]],
        address_line_1: ['', [Validators.maxLength(100)]],
        address_line_2: ['', [Validators.maxLength(100)]],
        city: ['', [Validators.maxLength(100)]],
        state: ['', [Validators.maxLength(100)]],
        postCode: ['', [Validators.maxLength(100)]],
        marticalStatus: ['', [Validators.maxLength(10)]],
        noOfChildren: ['', []],
        retired: [false, []],
        retired_date: ['', []],
        resigned_date: ['', []]
    });

    /**
     * Constructor
     */
    constructor(
        private formBuilder: FormBuilder,
        private commonService: CommonService,
        private router: Router,
        private commonFunction: CommonFunction,
        private matDialog: MatDialog) {
    }

    checkValue(event: any){
        console.log(event);
    }

    ngOnInit(): void {
        this.dob_range.setMonth(this.dob_range.getMonth() - 120);
        this.doh_range.setMonth(this.doh_range.getMonth() + 1);


        // get department
        let loginUserData: any = this.commonFunction.getLoginData();
        this.commonService.httpViaPost('cmpdepartmentlist', { "CompanyId": loginUserData.data.CompanyId }).subscribe((next: any) => {
            if (next.status == 1) {
                this.department = next.response;
            }
        });

        // get state
        this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.state = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.nationality = next.response;
            }
        });

    }

    changeState(event) {
        let data: any = {
            "StateListId": event.target.value,
            'CompanyId': this.commonService.companyId
        };

        this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.city = next.response;
            }
        });
    }

    keyPressIcPassport(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    keyPressPhoneNumber(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[0-9\-\/]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    addEmployeeFormSubmit() {
        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            if (this.addEmployeeForm.value.dateofhire != "") {
                this.addEmployeeForm.value.dateofhire = this.addEmployeeForm.value.dateofhire.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.date_of_birth != "") {
                this.addEmployeeForm.value.date_of_birth = this.addEmployeeForm.value.date_of_birth.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.phone) {
                let phone = this.addEmployeeForm.value.phone;
                phone.replace('-', '');
                phone = parseInt(phone);
                if (phone > 999999) {
                    this.addEmployeeForm.value.phone = "";
                }
            }

            console.log("this.addEmployeeForm.value : ", this.addEmployeeForm.value);


            let arr: any = [this.addEmployeeForm.value];

            let loginUserData: any = this.commonFunction.getLoginData();
            let data: any = {
                EmployeeDetails: arr,
                CompanyId: loginUserData.data.CompanyId,
                userId: loginUserData.data.Id
            };

            this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
                if (next.status == 1) {
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { name: "", animal: "" }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        console.log('The dialog was closed', result);
                        this.router.navigate(['/employee/add-employee']);
                    });
                }
            });
        } else {
            console.log("this.addEmployeeForm not valid.");
        }
    }

    formatDate(obj: any) {
        moment(obj).format('MM/DD/YYYY');
        return obj.toObject().date + '/' + parseInt(obj.toObject().months + 1) + '/' + obj.toObject().years;
    }

}








