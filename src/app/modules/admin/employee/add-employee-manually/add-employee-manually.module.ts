import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { Route, RouterModule } from '@angular/router';
import { AddEmployeeManuallyComponent } from 'app/modules/admin/employee/add-employee-manually/add-employee-manually.component';
import { MaterialModule } from '../../../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { FormsModule } from '@angular/forms';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../../../my-date-formats';

const exampleRoutes: Route[] = [
    {
        path     : '',
        component: AddEmployeeManuallyComponent
    }
];

@NgModule({
    declarations: [
        AddEmployeeManuallyComponent
    ],
    imports     : [
        MomentModule,
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild(exampleRoutes)
    ],
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class AddEmployeeManuallyModule
{
}
