import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmComponent } from 'app/core/component/confirm/confirm.component';
import { CommonService } from 'app/core/service/common.service';
import { Observable } from 'rxjs';


@Component({
    selector: 'company-list',
    templateUrl: './company-list.component.html',
    styleUrls: ['./company-list.component.scss']
})


export class CompanyListComponent implements OnInit {
    public companyData = [];
    public alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    public companyName = '';
    public pageCount = 1;
    public pageSize = 10;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public showNext;
    public showPrevious;

    constructor(
        private commonService: CommonService,
        private matDialog: MatDialog,
        private router: Router
    ) {
    }

    public ngOnInit(): void {
        this.searchCompany();
    }

    getCompanies(pageIndex: number): Observable<any> {
        const request = {
            'CoName': this.companyName,
            'PageIndex': pageIndex,
            'CompanyId': this.commonService.companyId
        };
        return this.commonService.httpViaPost('searchcompanylist', request);
    }

    searchCompany(): void {
        this.getCompanies(this.pageCount).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyData = next.response;

                if (this.companyData.length > 0) {
                    this.recordCount = this.companyData[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('searchcompanylist API Failed');
        });
    }

    editCompany(company): void {
        this.router.navigate(['/companies/edit-company'], { state: { data: company } });// JSON.stringify(company)]);
    }

    changePage(pageNumber): void {
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.searchCompany();
        }
    }

    filterCompanyList(alphabet: string): void {
        this.companyName = alphabet;
        this.searchCompany();
    }

    exportToExcel(): void {
        const exportObj = [];
        const pageIndex = 0;
        this.getCompanies(pageIndex).subscribe((response) => {
            if (response.status === 1 && response.response.length > 0) {
                response.response.forEach((report, index) => {
                    const element = {
                        'Sr No': index + 1,
                        'Name': report.CoName,
                        'Contact': report.ContactName,
                        'Mobile': report.ContactCellPhone,
                        'Email': report.Email,
                        'Status': report.Active == false ? 'In-Active' : 'Active',
                    };
                    exportObj.push(element);
                });
                this.commonService.downloadExcel(exportObj, 'Companies', 'Companies.xlsx');
            }
        }, (err) => {
            console.warn('Searchcompanylist API failed');
            this.getDummyData();
        });
    }

    getDummyData() {
        return { 'status': 1, 'message': 'Successful', 'response': [{ 'RecordCount': 50, 'CompanyUserLoginId': 'Admin', 'Passwd': 'Admin@123', 'Id': 10004, 'CoName': 'MUC Healthcare', 'Addr1': 'Kajang', 'Addr2': 'Kajang', 'City': 'Kajang', 'StateId': '1', 'EffectiveDt': '2021-10-25T00:00:00', 'Active': true, 'ExpirationDt': null, 'ContactName': 'Gurmeet Singh', 'ContactOfficePhone': null, 'ContactCellPhone': '60102035283', 'EnteredDt': '2021-10-25T00:00:00', 'UpdatedDt': null, 'EnteredUserId': '1', 'UpdatedUserId': null, 'OHSClient': true, 'OHSClientId': 1, 'sihatkuPanelCompCode': null, 'Email': null, 'ContactDesignation': null, 'PostalCode': null, 'ContactMailingAddress': null, 'IsActiveYN': null, 'InactiveDate': null }, { 'RecordCount': 50, 'CompanyUserLoginId': 'Tata', 'Passwd': 'Tata@', 'Id': 10002, 'CoName': 'Tata', 'Addr1': 'Jamesdpur', 'Addr2': 'Bokaro', 'City': 'Bokaro', 'StateId': '1', 'EffectiveDt': '2021-08-10T00:00:00', 'Active': true, 'ExpirationDt': null, 'ContactName': 'Tata', 'ContactOfficePhone': '7890654321', 'ContactCellPhone': '0987654321', 'EnteredDt': '2021-08-10T00:00:00', 'UpdatedDt': '2021-08-10T00:00:00', 'EnteredUserId': '1', 'UpdatedUserId': '1', 'OHSClient': true, 'OHSClientId': 4, 'sihatkuPanelCompCode': null, 'Email': null, 'ContactDesignation': null, 'PostalCode': null, 'ContactMailingAddress': null, 'IsActiveYN': null, 'InactiveDate': null }, { 'RecordCount': 50, 'CompanyUserLoginId': 'Hitachi2', 'Passwd': 'Hitachi2@', 'Id': 2, 'CoName': 'Hitachi', 'Addr1': 'bangi', 'Addr2': null, 'City': '36', 'StateId': 'KH', 'EffectiveDt': null, 'Active': false, 'ExpirationDt': null, 'ContactName': 'John', 'ContactOfficePhone': '9018278987', 'ContactCellPhone': null, 'EnteredDt': '2021-08-02T00:00:00', 'UpdatedDt': '2021-12-16T00:00:00', 'EnteredUserId': null, 'UpdatedUserId': null, 'OHSClient': false, 'OHSClientId': 0, 'sihatkuPanelCompCode': 0, 'Email': 'Hitachi@gmail.com', 'ContactDesignation': 'HR', 'PostalCode': null, 'ContactMailingAddress': null, 'IsActiveYN': null, 'InactiveDate': null }] }
    }
}
