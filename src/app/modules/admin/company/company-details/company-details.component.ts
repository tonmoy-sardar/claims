import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { ConfirmComponent } from 'app/core/component/confirm/confirm.component';
import { CommonService } from 'app/core/service/common.service';
import moment from 'moment';

@Component({
    selector: 'app-company-details',
    templateUrl: './company-details.component.html',
    styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
    public apiEHRSubDomain;
    public apiEHRBaseUrl;

    @Input() companyDetails;
    @Input() isEditMode = false;
    public companyForm: FormGroup;

    public stateList;
    public cityList;

    constructor(private formBuilder: FormBuilder,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private router: Router,
        private matDialog: MatDialog
    ) {
        this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
        this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
    }

    ngOnInit(): void {
        console.log(this.companyDetails)
        this.getStateList();
        this.initializeCompanyForm();
        if (this.companyDetails) {
            this.populateCompanyForm();
        }
    }


    initializeCompanyForm(): void {
        this.companyForm = this.formBuilder.group({
            CoId: ['', []],
            CoName: ['', [Validators.required, Validators.maxLength(50), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
            EnteredUserId: [{ value: '', disabled: this.isEditMode }, [Validators.required, Validators.minLength(8),
            Validators.maxLength(25), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
            UserId: ['', []],
            PassWord: ['', [Validators.required, Validators.maxLength(50)]],
            Addr1: ['', [Validators.maxLength(100)]],
            City: ['', [Validators.maxLength(100)]],
            StateId: ['', [Validators.maxLength(100)]],
            PostalCode: ['', [Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
            ContactName: ['', [Validators.required, Validators.pattern("^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$")]],
            ContactDesignation: ['', [Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
            Email: ['', [Validators.email]],
            ContactOfficePhone: ['', [Validators.maxLength(15), Validators.pattern('^[0-9\-\/]*$')]],
            ContactCellPhone: ['', [Validators.maxLength(15), Validators.pattern('^[0-9\-\/]*$')]],
            ContactMailingAddress: ['', []],
            OHSClient: ['', []],
            OHSClientId: ['', []],
            DOC: [{ value: '', disabled: this.isEditMode }, []],
            UpdatedDate: [{ value: '', disabled: this.isEditMode }, []]
        });
    }

    populateCompanyForm(): void {
        this.companyForm.patchValue({
            CoId: this.companyDetails.Id,
            CoName: this.companyDetails.CoName,
            EnteredUserId: this.companyDetails.CompanyUserLoginId,
            UserId: this.companyDetails.UserId,
            PassWord: this.companyDetails.Passwd,
            Addr1: this.companyDetails.Addr1,
            City: this.companyDetails.City,
            StateId: this.companyDetails.StateId,
            PostalCode: this.companyDetails.PostalCode,
            ContactName: this.companyDetails.ContactName,
            ContactDesignation: this.companyDetails.ContactDesignation,
            Email: this.companyDetails.Email,
            ContactOfficePhone: this.companyDetails.ContactOfficePhone,
            ContactCellPhone: this.companyDetails.ContactCellPhone,
            ContactMailingAddress: this.companyDetails.ContactMailingAddress,
            OHSClient: this.companyDetails.OHSClient,
            OHSClientId: this.companyDetails.OHSClientId,
            DOC: moment(this.companyDetails.EnteredDt).format('DD/MM/YYYY'),
            UpdatedDate: moment(this.companyDetails.UpdatedDt).format('DD/MM/YYYY'),
        });

        if (this.companyDetails.City && this.companyDetails.City !== '') {
            const data: any = {
                'stateListId': this.companyDetails.StateId
            };

            this.getCityList(data);
        }
    }

    companyFormSubmit(): void {
        this.addUpdateCompany();

        console.warn('Submit Called');
    }

    deleteCompany(status): void {
        const dialogRef = this.matDialog.open(ConfirmComponent, {
            width: '250px',
            data: status
        });

        dialogRef.afterClosed().subscribe((isDelete) => {
            console.log('The dialog was closed', isDelete);
            if (isDelete) {
                status = status == 'Active' ? 1 : 0;
                const request = {
                    'CoId': this.companyDetails.Id,
                    'Active': status
                };
                this.commonService.httpViaPost('inactivecompany', request).subscribe((next: any) => {
                    if (next.status === 1) {
                        const dialogRef = this.matDialog.open(AlertComponent, {
                            width: '250px',
                        });

                        dialogRef.afterClosed().subscribe((result) => {
                            console.log('The dialog was closed', result);
                            this.router.navigate(['/companies']);
                        });
                    }
                }, (err) => {
                    console.warn('inactivecompany Dummy Data');
                });
            }
        });
    }

    getStateList(): void {
        this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.stateList = next.response;
            }
        }, (err) => {
            this.getDummyStateData();
        }
        );
    }

    onStateChange(event): void {
        const data: any = {
            'stateListId': event.target.value,
            'CompanyId': this.commonService.companyId
        };

        this.getCityList(data);
    }

    getCityList(data: any): void {
        this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
            if (next.status === 1) {
                this.cityList = next.response;
            }
        }, (err) => {
            this.getDummyCityList();
        });
    }

    addUpdateCompany(): void {
        this.companyForm.markAllAsTouched();

        if (this.companyForm.valid) {
            if (!this.companyForm.value.CoId) {
                this.companyForm.value.CoId = 0;
            }
            this.companyForm.value.OHSClient = this.companyForm.value.OHSClient ? 1 : 0;
            this.companyForm.value.OHSClientId = +this.companyForm.value.OHSClientId;
            this.companyForm.value.UserId = this.commonFunction.getLoginData().data.UserName;
            this.companyForm.value.EnteredUserId = this.companyForm.get('EnteredUserId').value

            console.log('this.companyForm.value : ', this.companyForm.value);

            this.commonService.httpViaPost('addupdatecompany', this.companyForm.value).subscribe((next: any) => {
                if (next.status === 1) {
                    const actionTaken = this.isEditMode ? 'Updated' : 'Created';
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: `Company ${actionTaken} Successfully` }
                    });

                    dialogRef.afterClosed().subscribe((result) => {
                        console.log('The dialog was closed', result);
                        this.router.navigate(['/companies']);
                    });
                } else if (next.status === 0) {
                    const alertDialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Failure', message: next.message }
                    });

                    alertDialogRef.afterClosed().subscribe((result) => {
                        console.log('The dialog was closed', result);
                    });
                }
            });
        } else {
            this.showAlertOnAPIFailure();
        }
    }

    saveCompanyDetailsToEHR(): void {

        const req = {
            'sihatkuPanelCompCode': 2,
            'CompanyName': this.companyForm.value.CoName,
            'Street': this.companyForm.value.Addr1,
            'City': this.companyForm.value.City,
            'ccState': this.companyForm.value.StateId,
            'PostalCode': this.companyForm.value.PostalCode,
            'ContactName': this.companyForm.value.ContactName,
            'ContactDesignation': this.companyForm.value.ContactDesignation,
            'ContactOfficePhone': this.companyForm.value.ContactOfficePhone,
            'ContactMobile': this.companyForm.value.ContactCellPhone,
            'ContactEmail': this.companyForm.value.Email,
            'MailingAddress': this.companyForm.value.ContactMailingAddress,
            // 'AdvancePaymentPlan': 1,
            // 'AdvanceBalance': this.companyForm.value.CoName,
            // 'Remarks': this.companyForm.value.CoName,
            // 'Notes': this.companyForm.value.CoName,
            // 'AllowedPerVisit': this.companyForm.value.CoName,
            // 'UnlimitedPerVisit': 1,
            // 'CoversFamily': 1,
            // 'AllowedMonthlyLimit': this.companyForm.value.CoName,
            // 'AllowedYearlyLimit': this.companyForm.value.CoName,
            // 'AdvancePaymentDate': null,
            // 'Active': 1,
            'EnteredUserId': this.companyForm.value.EnteredUserId,
            // 'UpdatedUserId': ''
        };
        const url = `${this.apiEHRBaseUrl}Maintenance/SaveCompanyHRtoEHR?subdomain=${this.apiEHRSubDomain}`;
        this.commonService.httpPostWithoutHeaders(url, req, true).subscribe((response: any) => {
            if (response.data === 200) {
                const alertDialogRef = this.matDialog.open(AlertComponent, {
                    width: '250px'
                });

                alertDialogRef.afterClosed().subscribe((result) => {
                    console.log('The dialog was closed', result);
                    this.router.navigate(['/companies']);
                });
            } else {
                this.showAlertOnAPIFailure();
            }
        }, (err) => {
            this.showAlertOnAPIFailure();
            console.warn('UpadeClaimStatusHRToEHR API Failed');
        });
    }

    showAlertOnAPIFailure(): void {
        const alertDialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Failure', message: 'Operation was unsuccessful' }
        });

        alertDialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
        });
    }

    getDummyStateData(): void {
        this.stateList = [
            {
                'Id': 'JH',
                'State': 'Johor',
                'CountryListId': 'MY'
            },
            {
                'Id': 'KH',
                'State': 'Kedah',
                'CountryListId': 'MY'
            },
            {
                'Id': 'KL',
                'State': 'Kuala Lumpur',
                'CountryListId': 'MY'
            },
            {
                'Id': 'KN',
                'State': 'Kelantan',
                'CountryListId': 'MY'
            },
            {
                'Id': 'LA',
                'State': 'Labuan',
                'CountryListId': 'MY'
            },
            {
                'Id': 'ME',
                'State': 'Melaka',
                'CountryListId': 'MY'
            },
            {
                'Id': 'NS',
                'State': 'Negeri Sembilan',
                'CountryListId': 'MY'
            },
            {
                'Id': 'PG',
                'State': 'Pulau Pinang',
                'CountryListId': 'MY'
            },
            {
                'Id': 'PH',
                'State': 'Pahang',
                'CountryListId': 'MY'
            },
            {
                'Id': 'PJ',
                'State': 'Putrajaya',
                'CountryListId': 'MY'
            },
            {
                'Id': 'PK',
                'State': 'Perak',
                'CountryListId': 'MY'
            },
            {
                'Id': 'PL',
                'State': 'Perlis',
                'CountryListId': 'MY'
            },
            {
                'Id': 'SA',
                'State': 'Sabah',
                'CountryListId': 'MY'
            },
            {
                'Id': 'SK',
                'State': 'Sarawak',
                'CountryListId': 'MY'
            },
            {
                'Id': 'SL',
                'State': 'Selangor',
                'CountryListId': 'MY'
            },
            {
                'Id': 'TE',
                'State': 'Terengganu',
                'CountryListId': 'MY'
            }
        ];
    }

    getDummyCityList(): void {
        this.cityList = [
            {
                'Id': 86,
                'StateListId': 'JH',
                'City': 'Bakri'
            },
            {
                'Id': 50,
                'StateListId': 'JH',
                'City': 'Batu Pahat'
            },
            {
                'Id': 87,
                'StateListId': 'JH',
                'City': 'Buloh Kasap'
            },
            {
                'Id': 88,
                'StateListId': 'JH',
                'City': 'Chaah'
            },
            {
                'Id': 33,
                'StateListId': 'JH',
                'City': 'Johor Bahru'
            },
            {
                'Id': 89,
                'StateListId': 'JH',
                'City': 'Kampong Dungun'
            },
            {
                'Id': 90,
                'StateListId': 'JH',
                'City': 'Kelapa Sawit'
            },
            {
                'Id': 53,
                'StateListId': 'JH',
                'City': 'Kluang'
            },
            {
                'Id': 91,
                'StateListId': 'JH',
                'City': 'Kota Tinggi'
            },
            {
                'Id': 43,
                'StateListId': 'JH',
                'City': 'Kulai'
            },
            {
                'Id': 46,
                'StateListId': 'JH',
                'City': 'Labis'
            },
            {
                'Id': 92,
                'StateListId': 'JH',
                'City': 'Mersing'
            },
            {
                'Id': 52,
                'StateListId': 'JH',
                'City': 'Muar'
            },
            {
                'Id': 93,
                'StateListId': 'JH',
                'City': 'Parit Raja'
            },
            {
                'Id': 94,
                'StateListId': 'JH',
                'City': 'Pasir Gudang Baru'
            },
            {
                'Id': 95,
                'StateListId': 'JH',
                'City': 'Pekan Nenas'
            },
            {
                'Id': 54,
                'StateListId': 'JH',
                'City': 'Pontian'
            },
            {
                'Id': 51,
                'StateListId': 'JH',
                'City': 'Segamat'
            },
            {
                'Id': 47,
                'StateListId': 'JH',
                'City': 'Senai'
            },
            {
                'Id': 60,
                'StateListId': 'JH',
                'City': 'Simpang Renggam'
            },
            {
                'Id': 96,
                'StateListId': 'JH',
                'City': 'Taman Senai'
            },
            {
                'Id': 97,
                'StateListId': 'JH',
                'City': 'Tangkak'
            },
            {
                'Id': 98,
                'StateListId': 'JH',
                'City': 'Ulu Tiram'
            },
            {
                'Id': 56,
                'StateListId': 'JH',
                'City': 'Yong Peng'
            }
        ];
    }

}
