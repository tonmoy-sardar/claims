import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyRoutingModule } from './company-routing.module';
import { AddCompanyComponent } from './add-company/add-company.component';
import { MaterialModule } from 'app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditCompanyComponent } from './edit-company/edit-company.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
    declarations: [
        CompanyListComponent,
        AddCompanyComponent,
        EditCompanyComponent,
        CompanyDetailsComponent
    ],
    imports: [
        CommonModule,
        CompanyRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],

})

export class CompanyModule { }
