import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';

@Component({
    selector: 'app-add-company',
    templateUrl: './add-company.component.html',
    styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {
    // public stateList;
    // public cityList;
    // public addCompanyForm = this.formBuilder.group({
    //     CoId: ['', []],
    //     CoName: ['', [Validators.required, Validators.maxLength(50)]],
    //     Addr1: ['', [Validators.maxLength(100)]],
    //     City: ['', [Validators.maxLength(100)]],
    //     StateId: ['', [Validators.maxLength(100)]],
    //     PostalCode: ['', [Validators.maxLength(100)]],
    //     ContactName: ['', [Validators.required]],
    //     ContactDesignation: ['', []],
    //     Email: ['', []],
    //     ContactOfficePhone: ['', [Validators.maxLength(15), Validators.pattern('^[0-9\-\/]*$')]],
    //     ContactCellPhone: ['', [Validators.maxLength(15), Validators.pattern('^[0-9\-\/]*$')]],
    //     ContactMailingAddress: ['', []]
    // });

    constructor(private formBuilder: FormBuilder,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private router: Router,
        private matDialog: MatDialog
    ) { }

    ngOnInit(): void {
        // const t = this.router.getCurrentNavigation().extras.state;
        // this.getStateList();
    }

    // addCompanyFormSubmit(): void {
    //     this.addCompany();

    //     console.warn('Submit Called');
    // }

    // getStateList(): void {
    //     this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
    //         if (next.status === 1) {
    //             this.stateList = next.response;
    //         }
    //     }, (err) => {
    //         this.getDummyStateData();
    //     }
    //     );
    // }

    // onStateChange(event): void {
    //     const data: any = {
    //         'stateListId': event.target.value
    //     };

    //     this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
    //         if (next.status === 1) {
    //             this.cityList = next.response;
    //         }
    //     }, (err) => {
    //         this.getDummyCityList();
    //     });
    // }

    // addCompany(): void {

    //     this.addCompanyForm.markAllAsTouched();

    //     if (this.addCompanyForm.valid) {
    //         if (this.addCompanyForm.value.ContactCellPhone) {
    //             let contactCellPhone = this.addCompanyForm.value.ContactCellPhone;
    //             contactCellPhone.replace('-', '');
    //             contactCellPhone = parseInt(contactCellPhone, 10);
    //             if (contactCellPhone > 999999) {
    //                 this.addCompanyForm.value.ContactCellPhone = '';
    //             }
    //         }

    //         this.addCompanyForm.value.CoId = 0;

    //         console.log('this.addCompanyForm.value : ', this.addCompanyForm.value);

    //         this.commonService.httpViaPost('addupdatecompany', this.addCompanyForm.value).subscribe((next: any) => {
    //             if (next.status === 1) {
    //                 const dialogRef = this.matDialog.open(AlertComponent, {
    //                     width: '250px',
    //                     data: { name: '', animal: '' }
    //                 });

    //                 dialogRef.afterClosed().subscribe((result) => {
    //                     console.log('The dialog was closed', result);
    //                     this.router.navigate(['/companies']);
    //                 });
    //             }
    //         });
    //     } else {
    //         console.warn('this.addCompanyForm not valid.');
    //     }
    // }

    // getDummyStateData(): void {
    //     this.stateList = [
    //         {
    //             'Id': 'JH',
    //             'State': 'Johor',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'KH',
    //             'State': 'Kedah',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'KL',
    //             'State': 'Kuala Lumpur',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'KN',
    //             'State': 'Kelantan',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'LA',
    //             'State': 'Labuan',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'ME',
    //             'State': 'Melaka',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'NS',
    //             'State': 'Negeri Sembilan',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'PG',
    //             'State': 'Pulau Pinang',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'PH',
    //             'State': 'Pahang',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'PJ',
    //             'State': 'Putrajaya',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'PK',
    //             'State': 'Perak',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'PL',
    //             'State': 'Perlis',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'SA',
    //             'State': 'Sabah',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'SK',
    //             'State': 'Sarawak',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'SL',
    //             'State': 'Selangor',
    //             'CountryListId': 'MY'
    //         },
    //         {
    //             'Id': 'TE',
    //             'State': 'Terengganu',
    //             'CountryListId': 'MY'
    //         }
    //     ];
    // }

    // getDummyCityList(): void {
    //     this.cityList = [
    //         {
    //             'Id': 86,
    //             'StateListId': 'JH',
    //             'City': 'Bakri'
    //         },
    //         {
    //             'Id': 50,
    //             'StateListId': 'JH',
    //             'City': 'Batu Pahat'
    //         },
    //         {
    //             'Id': 87,
    //             'StateListId': 'JH',
    //             'City': 'Buloh Kasap'
    //         },
    //         {
    //             'Id': 88,
    //             'StateListId': 'JH',
    //             'City': 'Chaah'
    //         },
    //         {
    //             'Id': 33,
    //             'StateListId': 'JH',
    //             'City': 'Johor Bahru'
    //         },
    //         {
    //             'Id': 89,
    //             'StateListId': 'JH',
    //             'City': 'Kampong Dungun'
    //         },
    //         {
    //             'Id': 90,
    //             'StateListId': 'JH',
    //             'City': 'Kelapa Sawit'
    //         },
    //         {
    //             'Id': 53,
    //             'StateListId': 'JH',
    //             'City': 'Kluang'
    //         },
    //         {
    //             'Id': 91,
    //             'StateListId': 'JH',
    //             'City': 'Kota Tinggi'
    //         },
    //         {
    //             'Id': 43,
    //             'StateListId': 'JH',
    //             'City': 'Kulai'
    //         },
    //         {
    //             'Id': 46,
    //             'StateListId': 'JH',
    //             'City': 'Labis'
    //         },
    //         {
    //             'Id': 92,
    //             'StateListId': 'JH',
    //             'City': 'Mersing'
    //         },
    //         {
    //             'Id': 52,
    //             'StateListId': 'JH',
    //             'City': 'Muar'
    //         },
    //         {
    //             'Id': 93,
    //             'StateListId': 'JH',
    //             'City': 'Parit Raja'
    //         },
    //         {
    //             'Id': 94,
    //             'StateListId': 'JH',
    //             'City': 'Pasir Gudang Baru'
    //         },
    //         {
    //             'Id': 95,
    //             'StateListId': 'JH',
    //             'City': 'Pekan Nenas'
    //         },
    //         {
    //             'Id': 54,
    //             'StateListId': 'JH',
    //             'City': 'Pontian'
    //         },
    //         {
    //             'Id': 51,
    //             'StateListId': 'JH',
    //             'City': 'Segamat'
    //         },
    //         {
    //             'Id': 47,
    //             'StateListId': 'JH',
    //             'City': 'Senai'
    //         },
    //         {
    //             'Id': 60,
    //             'StateListId': 'JH',
    //             'City': 'Simpang Renggam'
    //         },
    //         {
    //             'Id': 96,
    //             'StateListId': 'JH',
    //             'City': 'Taman Senai'
    //         },
    //         {
    //             'Id': 97,
    //             'StateListId': 'JH',
    //             'City': 'Tangkak'
    //         },
    //         {
    //             'Id': 98,
    //             'StateListId': 'JH',
    //             'City': 'Ulu Tiram'
    //         },
    //         {
    //             'Id': 56,
    //             'StateListId': 'JH',
    //             'City': 'Yong Peng'
    //         }
    //     ];
    // }
}
