import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-edit-component',
    templateUrl: './edit-company.component.html',
    styleUrls: ['./edit-company.component.scss']
})
export class EditCompanyComponent implements OnInit {
    public editCompanyForm;
    constructor(private dataRoute: ActivatedRoute) { }

    ngOnInit(): void {
        this.editCompanyForm = history.state.data;
    }

}
