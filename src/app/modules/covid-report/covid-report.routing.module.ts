import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddSummaryComponent } from './add-summary/add-summary.component';
import { CovidReportComponent } from './covid-report/covid-report.component';
import { EditSummaryComponent } from './edit-summary/edit-summary.component';


const routes: Routes = [
    { path: '', component: CovidReportComponent },
    { path: 'add-summary', component: AddSummaryComponent },
    { path: 'edit-summary', component: EditSummaryComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CovidRoutingModule { }
