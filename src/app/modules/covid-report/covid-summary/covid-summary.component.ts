import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import moment from 'moment';

@Component({
  selector: 'app-covid-summary',
  templateUrl: './covid-summary.component.html',
  styleUrls: ['./covid-summary.component.scss']
})
export class CovidSummaryComponent implements OnInit {
  @Input() summaryData;
  @Input() isEditMode = false;
  public actionType = 'Add';
  public covidSummaryForm: FormGroup;
  public companyList = [];
  public maxDate = new Date();
  public minDate: any = new Date(2020, 4);

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private router: Router,
    private matDialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getCompanyList();
    this.initializeCovidSummaryForm();
    this.actionType = this.isEditMode ? 'Edit' : 'Add';

    if (this.summaryData) {
      this.populateCovidSummaryForm();
    }
  }

  initializeCovidSummaryForm(): void {
    this.covidSummaryForm = this.formBuilder.group({
      CompanyId: ['', []],
      TestType: ['', [Validators.required]],
      NoofEmpTested: ['', [Validators.pattern("^[0-9]*$")]],
      TestDate: ['', []],
      NoofPositives: ['', [Validators.pattern("^[0-9]*$")]],
      TestPrice: ['', [Validators.pattern("^[0-9.]*$")]],
      TotalInvoiceAmt: ['', [Validators.pattern("^[0-9.]*$")]]
    });
  }

  populateCovidSummaryForm(): void {
    this.covidSummaryForm.patchValue({
      CompanyId: this.summaryData.CompanyId,
      TestType: this.summaryData.TestType,
      NoofEmpTested: (this.summaryData.NoofEmpTested),
      TestDate: moment(this.summaryData.TestDate),
      NoofPositives: (this.summaryData.NoofPositives),
      TestPrice: this.summaryData.TestPrice,
      TotalInvoiceAmt: this.summaryData.TotalInvoiceAmt
    });
  }

  getCompanyList(): void {
    this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
      if (next.status === 1) {
        this.companyList = next.response;
      }
    }, (err) => {
      console.warn('getcompanyList API Failed.');
    });
  }


  covidSummaryFormSubmit(): void {
    this.addUpdateCovidSummary();

    console.warn('Submit Called');
  }
  addUpdateCovidSummary(): void {
    this.covidSummaryForm.markAllAsTouched();

    if (this.covidSummaryForm.valid) {
      console.log('this.covidSummaryForm.value : ', this.covidSummaryForm.value);

      const requestObject = Object.assign(this.covidSummaryForm.value);
      if (this.isEditMode) {
        requestObject.Id = this.summaryData.Id;
      }
      requestObject.NoofEmpTested = parseInt(requestObject.NoofEmpTested);
      requestObject.NoofPositives = parseInt(requestObject.NoofPositives);
      requestObject.CompanyId = requestObject.CompanyId.toString();
      requestObject.TestPrice = requestObject.TestPrice.toString();
      requestObject.TotalInvoiceAmt = requestObject.TotalInvoiceAmt.toString();

      this.commonService.httpViaPost('addupdatecovidsummary', this.covidSummaryForm.value).subscribe((next: any) => {
        if (next.status === 1) {
          let message = 'Covid Summary has been added successfully';
          if (this.isEditMode) {
            message = 'Covid Summary has been updated successfully';
          }
          const dialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Success', message: message }
          });
          dialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
            this.router.navigate(['/covid-report']);
          });
        } else if (next.status === 0) {
          this.showAlertOnAPIFailure();
        }
      });
    } else {
      this.showAlertOnAPIFailure();
    }
  }


  showAlertOnAPIFailure(): void {
    let message = 'Covid Summary could not be added';
    if (this.isEditMode) {
      message = 'Covid Summary could not be updated';
    }
    const alertDialogRef = this.matDialog.open(AlertComponent, {
      width: '250px',
      data: { title: 'Failure', message: message }
    });

    alertDialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

}
