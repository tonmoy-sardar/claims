import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'app/core/service/common.service';
import moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'covid-report',
    templateUrl: './covid-report.component.html',
    styleUrls: ['./covid-report.component.scss']
})
export class CovidReportComponent implements OnInit {
    public apiEHRSubDomain;
    public apiEHRBaseUrl;
    public companyId;
    public pageCount = 1;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public summaryReports = [];
    public companyList = [];
    public searchCompanyAutoComplete = new FormControl();
    public filteredCompanyList: Observable<string[]>;
    public companyObj;
    $companyList: Subject<any>;

    startDate: any = new Date(2020, 4);
    endDate: any = new Date();
    maxDate: any = new Date();
    public searchField: any = {
        TestType: '',
        FromDate: moment().startOf('M'),
        ToDate: moment(),
        CompanyId: '',
        IsAscending: '1',
        OrderByColumnName: '',
        pageNum: '1',
    };


    constructor(private commonService: CommonService,
        private router: Router
    ) {
        this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
        this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
        this.$companyList = new Subject();
    }

    ngOnInit(): void {
        this.$companyList.subscribe((next: any) => {
            this.getFilteredCompanies();
        });
        this.getCompanyList();
        this.sortData('TestDate', 'desc');
    }
    getFilteredCompanies(): any {
        this.filteredCompanyList = this.searchCompanyAutoComplete.valueChanges
            .pipe(
                startWith(''),
                map(value => this.filterCompanyList(value))
            );
    }

    filterCompanyList(value: any): any {
        let filterValue;
        if (typeof (value) === 'string') {
            filterValue = value.toLowerCase();
        } else {
            filterValue = value && value.CoName ? value.CoName.toLowerCase() : '';
        }
        return this.companyList.filter(option => option.CoName.toLowerCase().includes(filterValue));
    }

    getCompanyList(): void {
        this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyList = next.response;
                this.$companyList.next(this.companyList);
            }
        }, (err) => {
            console.warn('getcompanyList API Failed.');
        });
    }

    displayFn(company: any): string {
        return company && company.CoName ? company.CoName : '';
    }

    filterSummaryReports(): void {
        this.pageCount = 1;
        this.getSummaryReports();
    }

    getSummaryReports(): void {
        this.requestReportData().subscribe((data: any) => {
            if (data.status === 1) {
                this.summaryReports = data.response;
                if (this.summaryReports.length > 0) {
                    this.recordCount = this.summaryReports[0].TotalCount;
                } else {
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('CovidList API failed');
            this.getDummySummaryReports();
        });
    }

    requestReportData(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);

        if (this.searchField.FromDate && this.searchField.FromDate !== '') {
            searchField.FromDate = this.searchField.FromDate.format('DD/MM/YYYY');
        }

        if (this.searchField.ToDate && this.searchField.ToDate !== '') {
            searchField.ToDate = this.searchField.ToDate.format('DD/MM/YYYY');
        }

        if (this.companyObj) {
            if (this.companyObj.Id) {
                searchField.CompanyId = this.companyObj.Id.toString();
            } else {
                searchField.CompanyId = this.companyObj.toString();
            }
        }
        return this.commonService.httpViaPost('getcovidsummary', searchField);
    }

    resetSearch(): void {
        this.companyObj = null;
        this.resetPageIndex = true;
        this.pageCount = 1;
        this.searchField.TestType = '';
        this.searchField.FromDate = moment().startOf('M');
        this.searchField.ToDate = moment();
        this.searchField.CompanyId = '';
        this.searchField.OrderByColumnName = 'TestDate';
        this.searchField.IsAscending = 1;
        this.searchField.pageNum = 1;

        this.getSummaryReports();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getSummaryReports();
    }

    changePage(pageNumber): void {
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getSummaryReports();
        }
    }

    exportToExcel(): void {
        this.searchField.pageNum = 0;
        const exportObj = [];
        this.requestReportData().subscribe((response) => {
            if (response.status === 1 && response.response.length > 0) {
                response.response.forEach((report, index) => {
                    const element = {
                        'Sr No': index + 1,
                        'Test Date': report.TestDate,
                        'Company Name': report.CoName,
                        'No. Of Employees Tested': report.NoofEmpTested,
                        'No. Of Negatives': report.NoofNegatives,
                        'No. Of Positives': report.NoofPositives,
                        'Test Price': report.TestPrice,
                        'Total Invoice Amount(RM)': report.TotalInvoiceAmt
                    };
                    exportObj.push(element);
                });
                this.commonService.downloadExcel(exportObj, 'Covid_Summary_Report', 'Covid_Summary_Report.xlsx');
            }
        }, (err) => {
            console.warn('SummaryList API failed');
            this.getDummySummaryReports();
        });
    }

    public editCovidSummary(summaryData): void {
        this.router.navigate(['/covid-report/edit-summary'], { state: { data: summaryData } });// JSON.stringify(company)]);
    }

    
    changeEndDate(): any {
        if(this.searchField.FromDate == ''){
          return this.startDate;
        }else{
          return this.searchField.FromDate;
        }
      }


    getDummySummaryReports(): any {
        return {
            Total: 100,
            data: [
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                }
            ],
            msg: "Success",
            rcode: 200
        }
    }


}
