import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CommonFunction } from '../../core/classes/common-function';
import { CommonService } from '../../core/service/common.service';

function groupArrayOfObjects(list, key) {
    return list.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

@Component({
    selector: 'health-benifit-plan',
    templateUrl: './health-benifit-plan.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./health-benifit-plan.component.scss']
})
export class HealthBenifitPlanComponent {
    public displayedColumns: string[] = ['position', 'companyName',
        'silverMinPerVisit', 'silverMinPerMonth', 'silverMinPerYear',
        'goldMinPerVisit', 'goldMinPerMonth', 'goldMinPerYear',
        'platinumMinPerVisit', 'platinumMinPerMonth', 'platinumMinPerYear']
    public planDetails: any = [];
    public planDetailsDataSource: any = [];
    public companyList = [];
    public yearList: any = [];
    public selectedYear;

    public pageCount = 1;
    public showNext;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public searchCompanyAutoComplete = new FormControl();
    public filteredCompanyList: Observable<string[]>;
    public companyObj;
    $companyList: Subject<any>;

    public searchField: any = {
        selectedYear: '',
        CompanyId: '',
        Company: '',
        // CompanyName: '',
        IsAscending: '0',
        OrderByColumnName: '',
        PageNum: '1'
    };

    constructor(
        private _projectService: ProjectService,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
        this.$companyList = new Subject();
    }

    ngOnInit(): void {
        this.$companyList.subscribe((next: any) => {
            this.getFilteredCompanies();
        });
        this.getCompanyList();
        this.getYearList();
        this.sortData('CompanyName', 'desc');
    }

    getYearList(): void {
        const year = new Date().getFullYear();
        this.selectedYear = year;
        this.yearList.push(year);
        this.yearList.push(year + 1);
        console.warn(this.yearList);
    }

    getFilteredCompanies(): any {
        this.filteredCompanyList = this.searchCompanyAutoComplete.valueChanges
            .pipe(
                startWith(''),
                map(value => this.filterCompanyList(value))
            );
    }

    filterCompanyList(value: any): any {
        const filterValue = value && value.CoName ? value.CoName.toLowerCase() : '';
        return this.companyList.filter(option => option.CoName.toLowerCase().includes(filterValue));
    }

    getCompanyList(): void {
        this.commonService.httpViaGet('getcompanyList', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.companyList = next.response;
                this.$companyList.next(this.companyList);
            }
        }, (err) => {
            console.warn('getcompanyList API Failed.');
        });
    }

    displayFn(company: any): string {
        return company && company.CoName ? company.CoName : '';
    }

    getPlans() {
        this.searchField.PageNum = this.pageCount;
        this.requestHealthPlans().subscribe((data: any) => {
            if (data.status === 1) {
                this.planDetails = data.response;
                if (this.planDetails.length > 0) {
                    this.recordCount = this.planDetails[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('getmcreport API failed');
        });
    }

    requestHealthPlans(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);

        searchField.Flag = 4;
        if (this.searchField.Company) {
            if (this.searchField.Company.Id) {
                searchField.CompanyId = this.searchField.Company.Id.toString();
            }else{
                searchField.CompanyId = this.searchField.Company.toString();
            }
        }

        return this.commonService.httpViaPost('getcompanyhealthplan', searchField);
    }

    formatPlanDetails(): void {
        const groupedCompanies = groupArrayOfObjects(this.planDetails, "CompanyName");
        console.warn(groupedCompanies.CompanyName);

        this.planDetailsDataSource = groupedCompanies;
        console.warn((this.planDetails));
        console.warn((this.planDetailsDataSource));
    }

    filterHealthPlans(): void {
        this.pageCount = 1;
        this.getPlans();
    }

    resetSearch(): void {
        this.resetPageIndex = true;
        console.warn(this.searchField);

        this.pageCount = 1;
        this.searchField.CompanyId = '';
        this.searchField.Company = null;
        this.searchField.selectedYear = '';
        this.searchField.OrderByColumnName = 'CompanyName';
        this.searchField.IsAscending = 0;
        this.searchField.PageNum = 1;

        this.getPlans();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getPlans();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageCount += 1;
        //     this.showNext = false;
        // } else {
        //     this.pageCount -= 1;
        // }
        // this.getPlans();
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getPlans();
        }
    }


    getDummyPlanData(): void {
        let obj = [
            {
                "RecordCount": 4,
                "Id": 1,
                "PlanName": "Silver",
                "CompanyName": "GEODIS Malaysia Bhd",
                "CompanyId": 1,
                "HealthPlanId": 1,
                "CoverFamily": false,
                "SilverMinPerVisit": 10.00,
                "SilverMinPerMonth": 4.00,
                "SilverMinPerYear": 28.00,
                "GoldMinPerVisit": 764.00,
                "GoldMinPerMonth": 455.00,
                "GoldMinPerYear": 103.00,
                "PlatinumMinPerVisit": 240.00,
                "PlatinumMinPerMonth": 681.00,
                "PlatinumMinPerYear": 374.00,
                "SelectedYear": "2021",
                "EffectiveDt": "2021-08-17T00:00:00",
                "ExpirationDt": "2021-12-31T00:00:00",
                "FinalSubmitYN": "N",
                "EnteredUserId": "1"
            },
            {
                "CompanyId": 10004,
                "CompanyName": "MUC Healthcare",
                "SilverMinPerVisit": 100.00,
                "SilverMinPerMonth": 40.00,
                "SilverMinPerYear": 238.00,
                "GoldMinPerVisit": 76.00,
                "GoldMinPerMonth": 45.00,
                "GoldMinPerYear": 100.00,
                "PlatinumMinPerVisit": 20.00,
                "PlatinumMinPerMonth": 68.00,
                "PlatinumMinPerYear": 34.00,
                "SelectedYear": "2021-01-01T00:00:00",
                "EffectiveDt": "2021-01-01T00:00:00",
                "FinalSubmitYN": "N"
            }
        ];
        this.planDetails = obj;

    }

}
