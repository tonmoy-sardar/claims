/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/dashboard'
    },
    {
        id: 'companies',
        title: 'Companies',
        type: 'basic',
        icon: 'heroicons_outline:cube-transparent',
        link: '/companies',
        children: [
            {
                id: 'add-company',
                title: 'Add Company',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/companies/add-company'
            },
            {
                id: 'edit-company',
                title: 'Edit Company',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/companies/edit-company'
            }
        ]
    },
    {
        id: 'claims',
        title: 'Claims',
        type: 'aside',
        icon: 'heroicons_outline:receipt-tax',
        link: '/claims',
        children: [
            {
                id: 'auto-processing',
                title: 'Auto processing',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/claims/auto-processing'
            },
            {
                id: 'manual-processing',
                title: 'Manual processing',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/claims/manual-processing'
            },
            // {
            //     id   : 'monthly-visit-report',
            //     title: 'Monthly Visit Report',
            //     type : 'basic',
            //     icon : 'heroicons_outline:chart-pie',
            //     link : '/claims/monthly-visit-report'
            // }
        ]
    },
    {
        id: 'accounts-management',
        title: 'Accounts Management',
        type: 'aside',
        icon: 'heroicons_outline:clipboard-list',
        link: '/invoice-management',
        children: [
            {
                id: 'accounts-recievable',
                title: 'Accounts Receivable',
                type: 'basic',
                icon: 'heroicons_outline:arrow-down',
                link: 'accounts/receivable'
            },
            {
                id: 'company',
                title: 'Company Invoices',
                type: 'basic',
                icon: 'heroicons_outline:chart-square-bar',
                link: '/invoice-management/company'
            },
            {
                id: 'accounts-payable',
                title: 'Accounts Payable',
                type: 'basic',
                icon: 'heroicons_outline:arrow-up',
                link: 'accounts/payable'
            },
            {
                id: 'clinic-vouchers',
                title: 'Clinic Vouchers',
                type: 'basic',
                icon: 'heroicons_outline:color-swatch',
                link: '/invoice-management/clinic'
            },
        ]
    },
    {
        id: 'covid-report',
        title: 'Covid Report',
        type: 'basic',
        icon: 'heroicons_outline:globe',
        link: '/covid-report'
    },
    {
        id: 'health-benifit-plan',
        title: 'Health Plans',
        type: 'basic',
        icon: 'heroicons_outline:annotation',
        link: '/health-benifit-plan'
    },
    {
        id: 'notifications',
        title: 'Notification',
        type: 'basic',
        icon: 'heroicons_outline:speakerphone',
        link: '/notifications'
    },
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
