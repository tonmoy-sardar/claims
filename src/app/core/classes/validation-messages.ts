import { Injectable } from "@angular/core";

@Injectable()

export class ValidationMessages {
    public messages = [
        { type: 'emptyStringError', message: 'Please enter a valid input' },
        { type: 'pattern', message: 'Special characters are not allowed' },
        { type: 'emptySelection', message: 'Please select a value from the list' }
    ];
}