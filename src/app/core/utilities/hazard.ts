export const Hazards = {
    physicalHazard: [
        { id: 1, name: 'Noise', form: 'PhyHzNoise', value: false },
        { id: 2, name: 'Vibration', form: 'PhyHzVibration', value: false },
        { id: 3, name: 'Temperature (Heat Stress)', form: 'PhyHzTempHeatStress', value: false },
        { id: 4, name: 'Temperature (Cold Stress)', form: 'PhyHzTempColdStress', value: false },
        { id: 5, name: 'Radiation', form: 'PhyHzRadiation', value: false },
    ],
    biologicalHazard: [
        { id: 1, name: 'Bio Hazard' },
    ],
    ergonomicHazard: [
        { id: 1, name: 'Repetitive Motion' },
        { id: 2, name: 'Sustained Posture' },
    ],
    mentalHealthHazard: [
        { id: 1, name: 'Workplace Stress' },
        { id: 2, name: 'Workplace Bully' },
    ]
}