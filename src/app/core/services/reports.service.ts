import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend, HttpParams } from '@angular/common/http';
import { Observable, of, throwError, Subject, timer } from 'rxjs';
import { catchError, map, tap, switchMap, retry } from 'rxjs/operators';
// Handle error
import { HandleError, HandleErrorService } from './handle-error.service';
// Server Link
import { Router } from '@angular/router';

import { CommonFunction } from '../classes/common-function';
import { CommonService } from '../service/common.service';

// Content Type
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private handleError: HandleError;
  public apiUrl;
  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService,
    private router: Router,
    private commonFunction: CommonFunction,
    private commonService: CommonService
  ) {
    this.apiUrl = this.commonService.appConfig.SERVER_URL;
    this.handleError = handleErrorService.createHandleError('ReportsService');
  }

  getAbnormalExamResults(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetAbnormalExamResultsBL` + '?subdomain=mucohd';
    return this.http.post(apiPath, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleError('getAbnormalExamResults'))
      );
  }
  getMedicalSurSummaryList(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetMedSurSummaryListBL` + '?subdomain=mucohd';

    let loginUserData: any = this.commonFunction.getLoginData();
    console.log("loginUserData: ", loginUserData.data);
    /* set common header */
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'access_token': loginUserData.data.BearerToken,
        // 'Token': loginUserData.data.BearerToken,
        // 'Authorization': 'Bearer ' + loginUserData.data.BearerToken
      })
    };

    return this.http.post(apiPath, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalSurSummaryList'))
      );
  }
  getAudiometricReport(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetAudiometricReportBL` + '?subdomain=mucohd';
    return this.http.post(apiPath, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleError('getAudiometricReport'))
      );
  }
  getMedicalCertificateFitnessReport(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetMedicalCertificate`;
    return this.http.post(apiPath, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalCertificateFitnessReport'))
      );
  }
  getMedicalRemovalProtectionReport(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetMedicalRemoval`;
    return this.http.post(apiPath, data, httpOptions)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalRemovalProtectionReport'))
      );
  }

}
